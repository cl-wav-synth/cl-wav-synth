(in-package :line-synth)

(defun time->pos (time tmin tmax width)
  (float (* (- time tmin) (/ width (- tmax tmin)))))

(defun time->ipos (time tmin tmax width)
  (floor (* (- time tmin) (/ width (- tmax tmin)))))

(defun pos->time (pos tmin tmax width)
  (float (+ (* pos (/ (- tmax tmin) width)) tmin)))

(defun val->pos (val vmin vmax height)
  (float (* (- vmax val) (/ height (- vmax vmin)))))

(defun val->ipos (val vmin vmax height)
  (floor (* (- vmax val) (/ height (- vmax vmin)))))

(defun pos->val (pos vmin vmax height)
  (float (- vmax (* pos (/ (- vmax vmin) height)))))



(defun create-line (id vtt effect window &optional color points)
  (let* ((points (or points
		     (list->points `((0 ,(effect-default-value effect))
				     (1 ,(effect-default-value effect))))))
	 (p1 (first points))
	 (p2 (second points))
	 (t1 (lp-time p1))
	 (t2 (lp-time p2))
	 (v1 (lp-val p1))
	 (v2 (lp-val p2))
	 (a (/ (- v2 v1) (- t2 t1))))
    (make-line :id id :vtt vtt :effect effect :window window
	       :color (or color (->color (effect-color effect)))
	       :points points :t1 t1 :t2 t2 :v1 v1 :v2 v2 :a a
	       :vmin (effect-min effect) :vmax (effect-max effect))))

(defun reset-line (line)
  (setf (line-points line)
	(list->points `((0 ,(effect-default-value (line-effect line)))
			(1 ,(effect-default-value (line-effect line)))))))


(defun list->points (points)
  "Points is a list of ((time value) (time value) ...)"
  (loop :for p :in points
	:collect (make-lp :time (first p) :val (second p))))


(defun recompute-line (line time)
  (let (p1 p2)
    (dolist (p (line-points line))
      (setf p1 p2
	    p2 p)
      (when (> (lp-time p) time)
	(return)))
    (cond ((null p1) (setf (line-t1 line) 0
			   (line-t2 line) (lp-time p2)
			   (line-v1 line) (lp-val p2)
			   (line-v2 line) (lp-val p2)
			   (line-a line) 0))
	  ((> time (lp-time p2)) (setf (line-t1 line) (lp-time p2)
				       (line-t2 line) (lp-time p2)
				       (line-v1 line) (lp-val p2)
				       (line-v2 line) (lp-val p2)
				       (line-a line) 0))
	  (t (setf (line-t1 line) (lp-time p1)
		   (line-t2 line) (lp-time p2)
		   (line-v1 line) (lp-val p1)
		   (line-v2 line) (lp-val p2)
		   (line-a line) (/ (- (line-v2 line) (line-v1 line))
				    (- (line-t2 line) (line-t1 line))))))))



(defun line-point (line time)
  (unless (< (line-t1 line) time (line-t2 line))
    (recompute-line line time))
  (coerce (+ (* (line-a line) (- time (line-t1 line))) (line-v1 line)) 'single-float))


(defun test-line ()
  (let ((line (create-line 0 nil nil nil #xFFFFFF
			   (list (make-lp :time 1 :val 2)
				 (make-lp :time 2 :val 1)
				 (make-lp :time 3 :val 1.5)))))
    (format t "~A~%" line)
    (format t "~A~%" (line-point line 10))
    (loop for time from 0 to 5 by 0.1
	  do (format t "t=~A  ~A~%" time (line-point line time)))))


(defun draw-line (line &optional (draw-text t))
  (let* ((window (line-window line))
	 (tmin (window-tmin window))
	 (tmax (window-tmax window))
	 (width (xlib:drawable-width (window-xwindow window)))
	 (vmin (window-vmin window))
	 (vmax (window-vmax window))
	 (height (xlib:drawable-height (window-xwindow window)))
	 (is-select? (equal line *current-line*))
	 (rc (+ 3 (if is-select? 3 0)))
	 (rc/2 (floor (/ rc 2))))
    (when draw-text
      (set-gcontext window (line-color line) "black" (if is-select? :dash :solid) boole-1)
      (multiple-value-bind (sel-x1 sel-x2)
	  (draw-text window (format nil "(~A,~A,~A)"
				    (vtt-id (line-vtt line))
				    (effect-id (line-effect line))
				    (line-id line))
		     (vtt-color (line-vtt line)) nil is-select?)
	(declare (ignore sel-x2))
	(multiple-value-bind (sel-x3 sel-x4)
	    (draw-text window (effect-label-name (line-effect line)) (line-color line)
		       t is-select?)
	  (declare (ignore sel-x3))
	  (setf (line-sel-x1 line) sel-x1
		(line-sel-x2 line) sel-x4)))
      (when is-select?
	(set-gcontext window (line-color line) "black" :dash boole-1)
	(let ((val (val->ipos (line-vmin line) vmin vmax height)))
	  (xlib:draw-line (window-xwindow window) (window-xgc window)
			  0 val width val))
	(let ((val (val->ipos (line-vmax line) vmin vmax height)))
	  (xlib:draw-line (window-xwindow window) (window-xgc window)
			  0 val width val))))
    (let ((ltpos 0)
	  (lvpos (val->ipos (line-point line tmin) vmin vmax height))
	  (ntpos 0) (nvpos 0))
      (set-gcontext window (line-color line) "black"
		    (if is-select? :double-dash :solid)
		    (if is-select? boole-xor boole-1))
      (loop for p in (line-points line) do
	    (when (< tmin (lp-time p) tmax)
	      (setf ntpos (time->ipos (lp-time p) tmin tmax width)
		    nvpos (val->ipos (lp-val p) vmin vmax height))
	      (xlib:draw-line (window-xwindow window) (window-xgc window)
			      ltpos lvpos ntpos nvpos)
	      (xlib:draw-rectangle (window-xwindow window) (window-xgc window)
				   (- ltpos rc/2) (- lvpos rc/2) rc rc t)
	      (setf ltpos ntpos  lvpos nvpos)))
      (xlib:draw-rectangle (window-xwindow window) (window-xgc window)
			   (- ltpos rc/2) (- lvpos rc/2) rc rc t)
      (xlib:draw-line (window-xwindow window) (window-xgc window)
		      ltpos lvpos
		      width (val->ipos (line-point line tmax) vmin vmax height)))))



(defun add-point (time value)
  "Add a point (time, value) on the current line"
  (unless (warn-no-current-line)
    (let ((pos (position-if #'(lambda (pt) (> (lp-time pt) time)) (line-points *current-line*)))
	  (new-value (max (min value (line-vmax *current-line*)) (line-vmin *current-line*))))
      (setf (line-points *current-line*)
	    (if pos
		(nconc (subseq (line-points *current-line*) 0 pos)
		       (list (make-lp :time time :val new-value))
		       (subseq (line-points *current-line*) pos))
		(nconc (line-points *current-line*)
		       (list (make-lp :time time :val new-value)))))))
  (need-redisplay)
  nil)


(defun list-points ()
  "List all points in current line"
  (unless (warn-no-current-line)
    (format t "~A/~A: " (vtt-name (line-vtt *current-line*)) (effect-name (line-effect *current-line*)))
    (dolist (pt (line-points *current-line*))
      (format t "(~A, ~A) " (lp-time pt) (lp-val pt)))
    (format t "~%")))

(defun del-point (time)
  "Delete a point at time seconds on the current line"
  (unless (warn-no-current-line)
    (when (> (length (line-points *current-line*)) 2)
      (setf (line-points *current-line*)
	    (remove-if #'(lambda (pt) (= (lp-time pt) time)) (line-points *current-line*)))))
  (need-redisplay)
  nil)


(defun find-line-from-name (window x y)
  (when window
    (dolist (vtt *vtt-list*)
      (dolist (line (vtt-lines-list vtt))
	(when (and (equal window (line-window line))
		   (<= (line-sel-x1 line) x (line-sel-x2 line))
		   (is-in-name-p y))
	  (return-from find-line-from-name (values line vtt)))))))


(defun reorder-line-points (line)
  (setf (line-points line)
	(sort (line-points line)
	      (lambda (pt1 pt2)
		(< (lp-time pt1) (lp-time pt2))))))
