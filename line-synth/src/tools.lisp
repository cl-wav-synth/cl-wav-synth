(in-package :line-synth)



(defun need-redisplay ()
  (setf *need-redisplay* t))

(defun is-in-name-p (y)
  (< y *max-char-height*))


(defun set-max-char (font)
  (setf *max-char-width* (xlib:max-char-width *default-font*)
	*max-char-height* (+ (xlib:max-char-ascent font) (xlib:max-char-descent font))))


(defparameter *have-to-compress-notify* t
  "Config(): Compress event notify?
This variable may be useful to speed up some slow version of CLX.
It is particulary useful with CLISP/MIT-CLX.")

(defparameter *modifier-alias* '((:alt :mod-1)     (:alt-l :mod-1)
				 (:numlock :mod-2)
				 (:super_l :mod-4)
				 (:alt-r :mod-5)   (:alt-gr :mod-5)
				 (:capslock :lock))
  "Syntax: (modifier-alias effective-modifier)")


(defmacro awhen (test &body body)
  `(let ((it ,test))
     (when it
       ,@body)))

(defmacro aif (test then &optional else)
  `(let ((it ,test)) (if it ,then ,else)))



(defvar *%dbg-name%* "dbg")
(defvar *%dbg-count%* 0)
(defmacro dbg (&rest forms)
  `(progn
     ,@(mapcar #'(lambda (form)
		   (typecase form
		     (string `(setf *%dbg-name%* ,form))
		     (number `(setf *%dbg-count%* ,form))))
	       forms)
     (format t "~&DEBUG[~A - ~A]  " (incf *%dbg-count%*) *%dbg-name%*)
     ,@(mapcar #'(lambda (form)
		   (typecase form
		     ((or string number) nil)
		     (t `(format t "~A=~S   " ',form ,form))))
	       forms)
     (format t "~%")
     (force-output)
     ,@forms))




(defun read-preserve (&optional arg stream)
  (unless (listen *standard-input*)
    (format t "~:(~A~) = " arg))
  (let ((old-readtable-case (readtable-case *readtable*)))
    (setf (readtable-case *readtable*) :preserve)
    (prog1
	(read stream)
      (setf (readtable-case *readtable*) old-readtable-case))))

(defun to-string (string)
  (typecase string
    (string string)
    (t (format nil "~A" string))))


(defun create-symbol (&rest names)
  "Return a new symbol from names"
  (intern (string-upcase (apply #'concatenate 'string names))))


(defun selection-fun (selection fun ret)
  (cond ((and (sel-mark1 selection) (sel-mark2 selection))
	 (funcall fun (sel-mark1 selection) (sel-mark2 selection)))
	((sel-mark1 selection) (sel-mark1 selection))
	((sel-mark2 selection) (sel-mark2 selection))
	(t ret)))


(defun selection-min (selection &optional (ret most-positive-fixnum))
  (selection-fun selection #'min ret))

(defun selection-max (selection &optional (ret most-positive-fixnum))
  (selection-fun selection #'max ret))



;;(group-on-second '((A 1) (B 2) (C 1) (D 2) (E 3) (F 2)))
;; => ((E 3) (F D B 2) (C A 1))
;;(group-on-second '((A 1 x) (B 2 y) (C 1 x) (D 2 y) (E 3 z) (F 2 y) (G 2 y)))
;;=> ((E 3 Z) (G F D B 2 Y) (C A 1 X))
(defun group-on-second (list)
  (let ((acc nil))
    (dolist (l list)
      (let ((pos (position-if #'(lambda (x)
				  (equal  (second l) (car (last (butlast x))))) acc)))
	(if pos
	    (push (first l) (nth pos acc))
	    (push l acc))))
    acc))

(defun display-prompt (prompt)
  (format t "~A~A" (package-name *package*) prompt)
  (force-output))



(defun getenv (var)
  "Return the value of the environment variable."
  #+allegro (sys::getenv (string var))
  #+clisp (ext:getenv (string var))
  #+(or cmu scl)
  (cdr (assoc (string var) ext:*environment-list* :test #'equalp
              :key #'string))
  #+gcl (si:getenv (string var))
  #+lispworks (lw:environment-variable (string var))
  #+lucid (lcl:environment-variable (string var))
  #+(or mcl ccl) (ccl::getenv var)
  #+sbcl (sb-posix:getenv (string var))
  #+ecl (si:getenv (string var))
  #-(or allegro clisp cmu gcl lispworks lucid mcl sbcl scl ccl ecl)
  (error 'not-implemented :proc (list 'getenv var)))


(defun (setf getenv) (val var)
  "Set an environment variable."
  #+allegro (setf (sys::getenv (string var)) (string val))
  #+clisp (setf (ext:getenv (string var)) (string val))
  #+(or cmu scl)
  (let ((cell (assoc (string var) ext:*environment-list* :test #'equalp
							 :key #'string)))
    (if cell
        (setf (cdr cell) (string val))
        (push (cons (intern (string var) "KEYWORD") (string val))
              ext:*environment-list*)))
  #+gcl (si:setenv (string var) (string val))
  #+lispworks (setf (lw:environment-variable (string var)) (string val))
  #+lucid (setf (lcl:environment-variable (string var)) (string val))
  #+(or mcl ccl) (ccl::setenv var val)
  #+sbcl (sb-posix:putenv (format nil "~A=~A" (string var) (string val)))
  #+ecl (si:setenv (string var) (string val))
  #-(or allegro clisp cmu gcl lispworks lucid sbcl scl ccl mcl ecl)
  (error 'not-implemented :proc (list '(setf getenv) var)))



;;; XLIB Tools
(defun parse-display-string (display)
  "Parse an X11 DISPLAY string and return the host and display from it."
  (let* ((colon (position #\: display))
	 (host (subseq display 0 colon))
	 (rest (subseq display (1+ colon)))
	 (dot (position #\. rest))
	 (num (parse-integer (subseq rest 0 dot))))
    (values host num)))

(defun open-display (display-str protocol)
  (multiple-value-bind (host display-num) (parse-display-string display-str)
    (setf (getenv "DISPLAY") display-str)
    (xlib:open-display host :display display-num :protocol protocol)))



(let ((color-hash (make-hash-table :test 'equal)))
  (defun get-color (color)
    (multiple-value-bind (val foundp)
	(gethash color color-hash)
      (if foundp
	  val
	  (setf (gethash color color-hash)
		(xlib:alloc-color (xlib:screen-default-colormap *screen*) color))))))

(defgeneric ->color (color))

(defmethod ->color ((color-name string))
  color-name)

(defmethod ->color ((color integer))
  (labels ((hex->float (color)
	     (/ (logand color #xFF) 256.0)))
    (xlib:make-color :blue (hex->float color)
		     :green (hex->float (ash color -8))
		     :red (hex->float  (ash color -16)))))

(defmethod ->color ((color list))
  (destructuring-bind (red green blue) color
    (xlib:make-color :blue red :green green :red blue)))

(defmethod ->color ((color xlib:color))
  color)

(defmethod ->color (color)
  (format t "Wrong color type: ~A~%" color)
  "White")

(declaim (inline set-gcontext))

(defun set-gcontext (window foreground background line-style function)
  (setf (xlib:gcontext-foreground (window-xgc window)) (get-color foreground)
	(xlib:gcontext-background (window-xgc window)) (get-color background)
	(xlib:gcontext-line-style (window-xgc window)) line-style
	(xlib:gcontext-function (window-xgc window)) function))



(defmacro my-character->keysyms (ch)
  "Convert a char to a keysym"
  ;; XLIB:CHARACTER->KEYSYMS should probably be implemented in NEW-CLX
  ;; some day.  Or just copied from MIT-CLX or some other CLX
  ;; implementation (see translate.lisp and keysyms.lisp).  For now,
  ;; we do like this.  It suffices for modifiers and ASCII symbols.
  (if (fboundp 'xlib:character->keysyms)
      `(xlib:character->keysyms ,ch)
      `(list
       (case ,ch
	 (:character-set-switch #xFF7E)
	 (:left-shift #xFFE1)
	 (:right-shift #xFFE2)
	 (:left-control #xFFE3)
	 (:right-control #xFFE4)
	 (:caps-lock #xFFE5)
	 (:shift-lock #xFFE6)
	 (:left-meta #xFFE7)
	 (:right-meta #xFFE8)
	 (:left-alt #xFFE9)
	 (:right-alt #xFFEA)
	 (:left-super #xFFEB)
	 (:right-super #xFFEC)
	 (:left-hyper #xFFED)
	 (:right-hyper #xFFEE)
	 (t
	  (etypecase ,ch
	    (character
	     ;; Latin-1 characters have their own value as keysym
	     (if (< 31 (char-code ,ch) 256)
		 (char-code ,ch)
		 (error "Don't know how to get keysym from ~A" ,ch)))))))))




(defun char->keycode (char)
  "Convert a character to a keycode"
  (xlib:keysym->keycodes *display* (first (my-character->keysyms char))))


(defun keycode->char (code state)
  (xlib:keysym->character *display* (xlib:keycode->keysym *display* code 0) state))

(defun modifiers->state (modifier-list)
  (apply #'xlib:make-state-mask modifier-list))

(defun state->modifiers (state)
  (xlib:make-state-keys state))



(defun compress-motion-notify ()
  (when *have-to-compress-notify*
    (xlib:event-case (*display* :discard-p nil :peek-p t :timeout 0)
		     (:motion-notify () t))))


(defun clear-window (window)
  (setf (window-posx window) 0
	(window-posy window) (xlib:font-ascent *default-font*))
  (xlib:clear-area (window-xwindow window)))

(defun draw-text (window text foreground &optional (add-space t) selected)
  (let ((old-pos (window-posx window)))
    (set-gcontext window foreground "black" :solid boole-1)
    (when selected
      (xlib:draw-line (window-xwindow window) (window-xgc window)
		      (window-posx window) (window-posy window)
		      (+ (window-posx window) (* (xlib:max-char-width *default-font*) (length text)))
		      (window-posy window)))
    (xlib:draw-glyphs (window-xwindow window) (window-xgc window)
		      (window-posx window)
		      (window-posy window)
		      text)
    (incf (window-posx window) (* *max-char-width*
				  (+ (length text) (if add-space 1 0))))
    (values old-pos (window-posx window))))

 (defun draw-space (window)
   (incf (window-posx window) (xlib:max-char-width *default-font*)))

(defun draw-xy-text (window text x y foreground)
  (setf (xlib:gcontext-foreground (window-xgc window)) (get-color foreground))
  (xlib:draw-glyphs (window-xwindow window) (window-xgc window) x y text))





(defun find-window (xwindow)
  "Find a window from an xwindow"
  (when xwindow
    (dolist (window *window-list*)
      (when (xlib:window-equal xwindow (window-xwindow window))
	(return-from find-window window)))))

(defun key-query-pointer ()
  (multiple-value-bind (px py psame-screen-p pxwindow pstate-mask proot-x proot-y proot)
      (xlib:query-pointer *root*)
    (declare (ignore px py proot-x proot-y psame-screen-p pstate-mask proot))
    (when pxwindow
      (multiple-value-bind (x y same-screen-p xwindow state-mask root-x root-y root)
	  (xlib:query-pointer pxwindow)
	(declare (ignore root-x root-y same-screen-p xwindow state-mask root))
	(values pxwindow x y)))))



;;; Colors
(defun HSVtoRGB (h s v)
  (when (= s 0)
    (return-from HSVtoRGB (values v v v)))
  (let* ((h (/ h 60))
	 (i (floor h))
	 (f (- h i))
	 (p (* v (- 1 s)))
	 (q (* v (- 1 (* s f))))
	 (tt (* v (- 1 (* s (- 1 f))))))
    (case i
      (0 (values v tt p))
      (1 (values q v p))
      (2 (values p v tt))
      (3 (values p q v))
      (4 (values tt p v))
      (t (values v p q)))))

(defun float->RGB (h s v)
  (labels ((trunc (x)
	     (truncate (* x #xFF))))
    (multiple-value-bind (R G B)
	(HSVtoRGB h s v)
      (let* ((ret 0))
	(setf ret (trunc B))
	(setf (ldb (byte 8 8) ret) (trunc G))
	(setf (ldb (byte 8 16) ret) (trunc R))
	ret))))


(defun compute-color (n sat lum &optional (n-colors 100))
  (labels ((n->decal-n (n)
	     (let* ((max 10)
		    (mod (mod n max))
		    (den (truncate (/ n max))))
	       (+ (* mod max) den))))
    (float->RGB (* 360 (n->decal-n n) (/ 1 n-colors)) sat lum)))

(defparameter *current-color* -1)
(defun color ()
  (compute-color (incf *current-color*) 0.6 1 100))


;;; Time
(defun sec->hms (sec)
  (let* ((hour (truncate (/ sec 3600)))
	 (min (truncate (/ (- sec (* hour 3600)) 60)))
	 (sec (- sec (* min 60) (* hour 3600))))
      (values hour min sec)))

(defun sec->hms-string (sec)
  (multiple-value-bind (h m s)
      (sec->hms sec)
    (format nil "~2,'0D:~2,'0D:~6,3,,,'0F" h m s)))



(defun warn-no-current-line ()
  (unless (and *current-vtt* *current-line*)
    (format t "~&Warning: Current vtt and current line not set.~%")
    (display-prompt "> ")
    t))

(defun warn-no-current-vtt ()
  (unless *current-vtt*
    (format t "~&Warning: Current vtt not set.~%")
    (display-prompt "> ")
    t))

(defun find-free-number (l)		; stolen from stumpwm - thanks
  "Return a number that is not in the list l."
  (let* ((nums (sort l #'<))
	 (new-num (loop for n from 0 to (or (car (last nums)) 0)
		     for i in nums
		     when (/= n i)
		     do (return n))))
    (if new-num
	new-num
	;; there was no space between the numbers, so use the last + 1
	(if (car (last nums))
	    (1+ (car (last nums)))
	    0))))

