
;;(in-package :c-user)
;;
;;(defpackage libterminatorX
;;  (:use :common-lisp :cffi)
;;  (:export :effect-name
;;	   :effect-color
;;	   :tx-init
;;	   :tx-close
;;	   :tx-add-vtt
;;	   :tx-audio-on
;;	   :tx-audio-off
;;	   :tx-set-volume
;;	   :tx-set-pan
;;	   :tx-set-pitch
;;	   ;; Low Pass Filter
;;	   :tx-set-lp-enable
;;	   :tx-set-lp-gain
;;	   :tx-set-lp-reso
;;	   :tx-set-lp-freq
;;	   :my-cpp-function
;;	   :list-plugins
;;
;;	   :compute-color))
;;
;;(in-package :libterminatorX)

(in-package :line-synth)


;;(defun effect-name (effect-fun)
;;  (second (assoc effect-fun *effect-list*)))
;;
;;(defun effect-color (effect-fun)
;;  (third (assoc effect-fun *effect-list*)))


(define-foreign-library libterminatorX
    (:unix (:or "./libterminatorX.so" "libterminatorX.so"))
  (t (:default "libterminatorX.so")))

(use-foreign-library libterminatorX)


(defcfun ("init_terminatorX" tx-init) :void)
(defcfun ("close_terminatorX" tx-close) :void)

(defcfun ("add_vtt" tx-add-vtt) :pointer (filename :string))
(defcfun ("remove_all_vtt" tx-remove-all-vtt) :void)

(defcfun ("use_alsa" tx-use-alsa) :void)
(defcfun ("use_oss" tx-use-oss) :void)
(defcfun ("use_jack" tx-use-jack) :void)

(defcfun ("audio_on" tx-audio-on) :void)
(defcfun ("audio_off" tx-audio-off) :void)

(defcfun ("rec_on" tx-rec-on) :void)
(defcfun ("rec_off" tx-rec-off) :void)
(defcfun ("rec_to" tx-rec-to) :void (filename :string))

(defcfun ("my_cpp_function" my-cpp-function) :int (x :int))

(defcfun ("list_plugins" list-plugins) :void)

(defcfun ("add_vtt_fx" add-vtt-fx) :boolean (vtt :pointer) (ID :long))
(defcfun ("get_next_fx_control" get-next-fx-control) :pointer)

(defcfun ("get_fx_control_name" get-fx-control-name) :string (fx :pointer))
(defcfun ("get_fx_control_label_name" get-fx-control-label-name) :string (fx :pointer))
(defcfun ("get_fx_control_value" get-fx-control-value) :float (fx :pointer))
(defcfun ("get_fx_control_min_value" get-fx-control-min-value) :float (fx :pointer))
(defcfun ("get_fx_control_max_value" get-fx-control-max-value) :float (fx :pointer))
(defcfun ("get_fx_control_scale_value" get-fx-control-scale-value) :float (fx :pointer))
(defcfun ("set_fx_control_value" set-fx-control-value) :void (fx :pointer) (value :float))



(defmacro define-effect (id fun-name name label-name color default min max)
  (let ((tx-symbol (intern (string-upcase (substitute #\- #\_ (format nil "tx-~A" fun-name))))))
    `(progn
      (defcfun (,fun-name ,tx-symbol) :void (vtt :pointer) (arg :float))
      (push (make-effect :id ,id
			 :fun (function ,tx-symbol)
			 :name ,name :label-name ,label-name
			 :param nil
			 :color ,color :default-value ,default
			 :min ,min :max ,max)
	    *effect-list*))))

(defun find-effect-by-fun (fun)
  (dolist (effect *effect-list*)
    (when (equal fun (effect-fun effect))
      (return-from find-effect-by-fun (copy-effect effect)))))

(define-effect -1 "set_volume" "Volume" "Volume" (color) 0 -3 3)
(define-effect -1 "set_pan" "Pan" "Pan" (color) 0 -1 1)
(define-effect -1 "set_pitch" "Pitch" "Pitch" (color) 1 -10 10)

(define-effect -2 "set_lp_enable" "Builtin LowPass Filter - Enable" "LP-Enable" (color) 0 0 1)
(define-effect -2 "set_lp_freq" "Builtin LowPass Filter - Frequency" "LP-Freq" (color) 0.3 0 1)
(define-effect -2 "set_lp_gain" "Builtin LowPass Filter - Gain" "LP-Gain" (color) 1 0 2)
(define-effect -2 "set_lp_reso" "Builtin LowPass Filter - Resonance" "LP-Reso" (color) 0.8 0 1)

(define-effect -3 "set_ec_enable" "Builtin Echo - Enable" "EC-Enable" (color) 0 0 1)
(define-effect -3 "set_ec_length"   "Builtin Echo - Length" "EC-Length" (color) 0.5 0 1)
(define-effect -3 "set_ec_feedback"   "Builtin Echo - Feedback" "EC-Feedback" (color) 0.3 0 1)
(define-effect -3 "set_ec_volume"   "Builtin Echo - Volume" "EC-Volume" (color) 1 0 3)
(define-effect -3 "set_ec_pan"   "Builtin Echo - Pan" "EC-Pan" (color) 0 -1 1)


(defun extract-fx-control-label-name (control)
  (let ((name (get-fx-control-label-name control)))
    (subseq name 0 (or (position #\space name) (length name)))))
