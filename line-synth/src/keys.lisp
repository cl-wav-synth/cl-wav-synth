(in-package :line-synth)

(defparameter *key-binding* nil)
(defparameter *mouse-binding* nil)

(defparameter *default-modifiers* '()
  "Config(): Default modifiers list to append to explicit modifiers
Example: :mod-2 for num_lock, :lock for Caps_lock...")

(defparameter *fun-press* #'first)
(defparameter *fun-release* #'second)


(defun with-capslock ()
  (pushnew :lock *default-modifiers*))

(defun without-capslock ()
  (setf *default-modifiers* (remove :lock *default-modifiers*)))

(defun with-numlock ()
  (pushnew :mod-2 *default-modifiers*))

(defun without-numlock ()
  (setf *default-modifiers* (remove :mod-2 *default-modifiers*)))


(defmacro define-init-hash-table-key (hash-table name)
  (let ((init-name (create-symbol "init-" (format nil "~A" hash-table))))
    `(progn
      (defun ,init-name ()
	(setf ,hash-table (make-hash-table :test 'equal))
	(setf (gethash 'name ,hash-table) ,name))
      (,init-name))))

(define-init-hash-table-key *key-binding* "Key bindings")
(define-init-hash-table-key *mouse-binding* "Mouse bindings")

(defun unalias-modifiers (list)
  (dolist (mod *modifier-alias*)
    (setf list (substitute (second mod) (first mod) list)))
  list)

(defun key->list (key)
  (list (first key) (modifiers->state (append (unalias-modifiers (rest key))
					      (unalias-modifiers *default-modifiers*)))))

(defmacro define-define-key (name hashtable)
  (let ((name-key-fun (create-symbol "define-" name "-key-fun"))
	(name-key (create-symbol "define-" name "-key"))
	(undefine-name-fun (create-symbol "undefine-" name "-key-fun"))
	(undefine-name (create-symbol "undefine-" name "-key"))
	(undefine-multi-name (create-symbol "undefine-" name "-multi-keys")))
    `(progn
      (defun ,name-key-fun (key function &rest args)
	"Define a new key, a key is '(char modifier1 modifier2...))"
	(setf (gethash (key->list key) ,hashtable) (list function args)))
      (defmacro ,name-key ((key &rest modifiers) function &rest args)
	`(,',name-key-fun (list ,key ,@modifiers) ,function ,@args))
      (defun ,undefine-name-fun (key)
	"Undefine a new key, a key is '(char modifier1 modifier2...))"
	(remhash (key->list key) ,hashtable))
      (defmacro ,undefine-name ((key &rest modifiers))
	`(,',undefine-name-fun (list ,key ,@modifiers)))
      (defmacro ,undefine-multi-name (&rest keys)
	`(progn
	  ,@(loop for k in keys
		  collect `(,',undefine-name ,k)))))))



(defmacro define-define-mouse (name hashtable)
  (let ((name-mouse-fun (create-symbol "define-" name "-fun"))
	(name-mouse (create-symbol "define-" name))
	(undefine-name (create-symbol "undefine-" name)))
    `(progn
       (defun ,name-mouse-fun (button function-press &optional function-release &rest args)
	 "Define a new mouse button action, a button is '(button number '(modifier list))"
	 (setf (gethash (key->list button) ,hashtable) (list function-press function-release args)))
       (defmacro ,name-mouse ((button &rest modifiers) function-press &optional function-release &rest args)
	 `(,',name-mouse-fun (list ,button ,@modifiers) ,function-press ,function-release ,@args))
       (defmacro ,undefine-name ((key &rest modifiers))
	 `(remhash (list ,key ,@modifiers) ,',hashtable)))))


(define-define-key "main" *key-binding*)
(define-define-mouse "mouse" *mouse-binding*)













(defun find-key-from-code (hash-table-key code state)
  "Return the function associated to code/state"
  (labels ((function-from (key &optional (new-state state))
	     (multiple-value-bind (function foundp)
		 (gethash (list key new-state) hash-table-key)
	       (when (and foundp (first function))
		 function)))
	   (from-code ()
	     (function-from code))
	   (from-char ()
	     (let ((char (keycode->char code state)))
	       (function-from char)))
	   (from-string ()
	     (let ((string (keysym->keysym-name (xlib:keycode->keysym *display* code 0))))
	       (function-from string)))
	   (from-string-shift ()
	     (let* ((modifiers (state->modifiers state))
		    (string (keysym->keysym-name (xlib:keycode->keysym *display* code (cond  ((member :shift modifiers) 1)
											     ((member :mod-5 modifiers) 2)
											     (t 0))))))
	       (function-from string)))
	   (from-string-no-shift ()
	     (let* ((modifiers (state->modifiers state))
		    (string (keysym->keysym-name (xlib:keycode->keysym *display* code (cond  ((member :shift modifiers) 1)
											     ((member :mod-5 modifiers) 2)
											     (t 0))))))
	       (function-from string (modifiers->state (remove :shift modifiers))))))
    (or (from-code) (from-char) (from-string) (from-string-shift) (from-string-no-shift))))



(defun funcall-key-from-code (hash-table-key code state &rest args)
  (let ((function (find-key-from-code hash-table-key code state)))
    (when function
      (apply (first function) (append args (second function)))
      t)))


(defun funcall-button-from-code (hash-table-key code state window x y
				 &optional (action *fun-press*) args)
  (let ((state (modifiers->state (set-difference (state->modifiers state)
						 '(:button-1 :button-2 :button-3 :button-4 :button-5)))))
    (multiple-value-bind (function foundp)
	(gethash (list code state) hash-table-key)
      (if (and foundp (funcall action function))
	  (progn
	    (apply (funcall action function) `(,window ,x ,y ,@(append args (third function))))
	    t)
	  nil))))



(defun handle-key-press (&rest event-slots &key root code state &allow-other-keys)
  (declare (ignore event-slots root))
  (funcall-key-from-code *key-binding* code state))

(defun handle-button-press (&rest event-slots &key code state window x y &allow-other-keys)
  (declare (ignore event-slots))
  (funcall-button-from-code *mouse-binding* code state window x y *fun-press*))

(defun handle-button-release (&rest event-slots &key code state window x y &allow-other-keys)
  (declare (ignore event-slots))
  (funcall-button-from-code *mouse-binding* code state window x y *fun-release*))


(defun pending-motion? ()
  (xlib:event-case (*display* :discard-p nil :peek-p t :timeout 0)
		   (:motion-notify () (return-from pending-motion? t)))
  nil)

(defun handle-motion-notify (&rest event-slots &key window x y &allow-other-keys)
  (declare (ignore event-slots))
  (unless (pending-motion?)
    (funcall-button-from-code *mouse-binding* 'motion 0 window x y *fun-press*)))



(defun display-bindings (hash)
  (format t "~A:~%" (gethash 'NAME hash))
  (maphash #'(lambda (key val)
	       (when (consp key)
		 (format t "  ~:(~{~A+~}~)~:(~A~) ~30T ~A~%"
			 (state->modifiers (second key))
			 (first key)
			 (documentation (first val) 'function))))
	   hash)
  (terpri))



(defun ls-bindings ()
  "Display all bindings"
  (display-bindings *key-binding*)
  (display-bindings *mouse-binding*)
  (values))


