(in-package :line-synth)

(defparameter *command-list* nil)

(defmacro defcommand (name args &body body)
  (typecase name
    (symbol `(push (list ',name (defun ,(intern (format nil "CMD-~S" name)) ,args ,@body) ',args)
		   *command-list*))
    (list `(let ((fn (defun ,(intern (format nil "CMD-~S" (first name))) ,args ,@body)))
	     ,@(loop for n in name
		  collect `(push (list ',n fn ',args) *command-list*))))))






(defcommand help ()
  "Show this help"
  (dolist (cmd (group-on-second *command-list*))
    (format t ". ~A~%    ~A~%" (format nil "~{~:(~A~)~^, ~} ~:(~A~)" (butlast (butlast cmd))
				       (aif (car (last cmd)) it ""))
	    (documentation (first (last (butlast cmd))) 'function))))

(defcommand (done bye quit) ()
  "Leave line-synth"
  (throw 'exit-loop nil))

(defcommand (lsb lsbind ls-binding binding) ()
  "List current keys and mouse bindings"
  (ls-bindings))

(defcommand use-alsa ()
  "Use alsa as audio device"
  (tx-use-alsa))

(defcommand use-oss ()
  "Use oss as audio device"
  (tx-use-oss))

(defcommand use-jack ()
  "Use jack as audio device"
  (tx-use-jack))

(defcommand (audio-on on play) ()
  "Turn audio on"
  (unless *audio-playing*
    (start-timer (if (sel-window *selection*)
		     (selection-min *selection* 0)
		     0))
    (tx-audio-on)))


(defcommand (audio-off off stop) ()
  "Turn audio off"
  (when *audio-playing*
    (stop-timer)
    (tx-audio-off)))

(defcommand (toggle-audio tg pause) ()
  "Start/Stop playing"
  (if *audio-playing*
      (cmd-audio-off)
      (cmd-audio-on)))

(defcommand (recon rec-on ron) ()
  "Start recording"
  (tx-rec-on))

(defcommand (recoff rec-off roff) ()
  "Stop recording"
  (tx-rec-off))

(defcommand (recto rec-to rto) (filename)
  "Set the recording filename"
  (tx-rec-to (to-string filename)))


(defcommand (delay set-delay) (delay)
  "Set the update delay"
  (set-delay delay))


(defcommand (add-vtt addv) (vtt-filename)
  "Add a virtual turntable"
  (when (add-vtt vtt-filename) t))

(defcommand (onv on-vtt) (name)
  "Set the current vtt"
  (set-current-vtt name))

(defcommand (onl on-line) (name)
  "Set the current line"
  (set-current-line name))

(defcommand (onvl on-vtt-line) (vtt-name line-name)
  "Set the current wtt and the current line"
  (set-current-vtt vtt-name)
  (set-current-line line-name))

(defcommand (onid on-id) (vtt-id effect-id line-id)
  "Select a line by its IDs"
  (select-line-by-id vtt-id effect-id line-id))

(defcommand (onvid on-vtt-id) (vtt-id)
  "Select a vtt by its IDs"
  (select-vtt-by-id vtt-id))



(defcommand (pwd pvl) ()
  "Print current vtt and effect"
  (format t "Current VTT: ~A    Current Line: ~A~%"
	  (if *current-vtt* (format nil "~A, ~A" (vtt-id *current-vtt*) (vtt-name *current-vtt*)) "NONE")
	  (if *current-line* (effect-name (line-effect *current-line*)) "NONE")))

(defcommand (lsv ls-vtt) ()
  "List all vtt"
  (dolist (vtt *vtt-list*)
    (format t "VTT: ~A, ~A~%" (vtt-id vtt) (vtt-name vtt))))

(defun display-effects (vtt)
  (dolist (line (vtt-lines-list vtt))
    (format t "  Effect: ~A ~A~%" (effect-name (line-effect line))
	    (if (and (equal vtt *current-vtt*)
		     (equal line *current-line*))
		"<- *Current*" ""))))

(defcommand (lse ls-effect) ()
  "List all effect"
  (when *current-vtt*
    (format t "VTT: ~A, ~A~%" (vtt-id *current-vtt*) (vtt-name *current-vtt*))
    (display-effects *current-vtt*)))

(defcommand ls ()
  "List all vtt and effects"
  (dolist (vtt *vtt-list*)
    (format t "VTT: ~A, ~A~%" (vtt-id vtt) (vtt-name vtt))
    (display-effects vtt)))

(defcommand (add addp add-point) (time value)
  "Add a point (time, value) on the current line"
  (add-point time value))

(defcommand (ls-point lsp) ()
  "List all points in he current line"
  (list-points))

(defcommand (del delp delete-point) (time)
  "Delete a point at time seconds on the current line"
  (del-point time))

(defcommand (list-plugins lspl lsfx) ()
  "List all availables plugins"
  (list-plugins))

(defcommand (add-fx addfx) (effect-id)
  "Add an effect on the current vtt"
  (add-fx-on-current-vtt effect-id))

(defcommand (del-fx delfx rmfx rm-fx) ()
  "Remvoe the curent effect on the current vtt"
  (remove-current-fx-on-current-vtt))


(defcommand (new clear) ()
  "Start a new line-synth session"
  (clear-all))

(defcommand (save) (filename)
  "Save the current line-synth state"
  (save-state filename))

(defcommand (load restore) (filename)
  "Load a saved line-synth state"
  (load-state filename))

(defcommand (reset-line reset raz) ()
  "Reset the current line to its defaults values"
  (reset-line *current-line*)
  (need-redisplay))

;;; Triangles commands
(defcommand (triangle tri) (t-start t-period n-period min max)
  "Add a triangle on the current line"
  (triangle-on-current-line t-start t-period n-period min max))

(defcommand (sym-triangle sym-tri) (t-start t-period n-period min max)
  "Add a symmetric triangle on the current line"
  (sym-triangle-on-current-line t-start t-period n-period min max))

(defcommand (gen-triangle gen-tri) (t-start t-period n-period min max duty)
  "Add a triangle with duty on the current line"
  (gen-triangle-on-current-line t-start t-period n-period min max duty))


;;; Square commands
(defcommand (square sqr) (t-start t-period n-period min max)
  "Add a square on the current line"
  (square-on-current-line t-start t-period n-period min max))

(defcommand (gen-square gen-sqr) (t-start t-period n-period min max duty)
  "Add a square with duty on the current line"
  (gen-square-on-current-line t-start t-period n-period min max duty))


;;; Sinus function
(defcommand (sin sinus) (t-start t-period n-period min max prec)
  "Add a sinus on the current line"
  (sinus-on-current-line t-start t-period n-period min max prec))


;;; Copy functions
(defcommand (copy cp) (vtt-id effect-id line-id)
  "Copy a line integrally to the current line"
  (copy-line-by-id vtt-id effect-id line-id))

(defcommand (copy-time cpt) (vtt-id effect-id line-id t-start t-end)
  "Copy a line from t-start to t-end to the current line"
  (copy-line-by-id-time vtt-id effect-id line-id t-start t-end))

(defcommand (repeat rp) (t-start t-end t-begin n-repeat t-space)
  "Repeat n-repeat times points between t-start and t-end beginning
at t-begin leaving t-space between each repeated points."
  (repeat-points t-start t-end t-begin n-repeat t-space))


;;; Do macro... Just for the help
(defcommand (do-lp do-lp-t do-lp-sel do-all-lp) ()
  "Apply code to all points in current line. Call this command like this:
    (do-lp ...your code...)
    (do-lp-t (t-start t-end) ...your code...)
    (do-lp-sel ...your code...)
    The variable 'it' is the current point, 'time' is the point time and
    'val' is the point value
    do-lp-t apply code only when current point time is between t-start and t-end.
    do-lp-sel apply code only when current point is between selection markers.
    do-all-lp apply code on all points on all lines on all vtt."
  'nothing-to-do)


;;(defcommand (test tst tst1) (foo bar baz)
;;  "A test function"
;;  (values foo bar baz))
;;
;;
;;(defcommand simple-test (foo bar baz)
;;  "A simple test function"
;;  (values foo bar baz))


(defun eval-command (cmd)
  (let ((result (assoc cmd *command-list*)))
    (when result
      (awhen (multiple-value-list
	      (apply (second result)
		     (loop for arg in (car (last result))
			collect (read-preserve arg *standard-input*))))
	(format t "~{~S~%~}" it))
      t)))



(defun do-repl (&optional (prompt "> "))
  (when (listen *standard-input*)
    (handler-case
	(let ((cmd (read *standard-input*)))
	  (unless (eval-command cmd)
	    (format t "~{~S~%~}" (multiple-value-list (eval cmd)))
	    (force-output)))
      (error (c)
	(format t "~A~%" c)
	(force-output)))
    (clear-input)
    (display-prompt prompt)))
