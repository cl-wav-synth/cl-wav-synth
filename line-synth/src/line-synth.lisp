(in-package :line-synth)


(defun done ()
  "Exit"
  (throw 'exit-loop nil))


(defun toggle-current-line-lock ()
  "Toggle the current line lock"
  (setf *current-line-lock* (not *current-line-lock*))
  (need-redisplay))


(defun redraw-all ()
  (dolist (window *window-list*)
    (clear-window window)
    (draw-axis window))
  (draw-selection)
  (dolist (vtt *vtt-list*)
    (draw-vtt vtt))
  (dolist (window *window-list*)
    (draw-cursor window nil)))

(defun call-init-hook ()
  ;;(load-state "complex.lp")
  ;;(add-vtt "synth.wav")
  ;;(add-vtt "bomb.wav")
  ;;(add-fx-on-current-vtt 1185))
  ;;  (load-state "test.lp")
  ;;  (setf (sel-window *selection*) (first *window-list*)
  ;;	(sel-mark1 *selection*) 8
  ;;	(sel-mark2 *selection*) 30))
  )


(defun handle-exposure (&rest event-slots &key window &allow-other-keys)
  (declare (ignore event-slots))
  (dolist (win *window-list*)
    (when (equal window (window-xwindow win))
      (clear-window win)
      (draw-axis win)))
  (when (and (sel-window *selection*) (equal window (window-xwindow (sel-window *selection*))))
    (draw-selection))
  (dolist (vtt *vtt-list*)
    (draw-vtt-xwindow vtt window))
  (dolist (win *window-list*)
    (when (equal window (window-xwindow win))
      (draw-cursor win nil))))



(defun run (&optional (display (or (getenv "DISPLAY") ":0")) protocol)
  (setf *display* (open-display display protocol)
	*screen* (xlib:display-default-screen *display*)
	*root* (xlib:screen-root *screen*)
	*default-font* (xlib:open-font *display* *default-font-string*)
	*selection* (make-sel :window nil :mark1 nil :mark2 nil))
  (set-max-char *default-font*)
  (tx-init)
  (call-init-hook)
  (display-prompt "> ")
  (redraw-all)
  (catch 'exit-loop
    (loop
       (when *need-redisplay*
	 (redraw-all)
	 (setf *need-redisplay* nil))
       (when (update-needed)
	 (update-cursors)
	 (update-vtt))
       (xlib:process-event *display* :handler (lambda (&rest event-slots &key event-key &allow-other-keys)
						(case event-key
						  (:key-press (apply #'handle-key-press event-slots))
						  (:button-press (apply #'handle-button-press event-slots))
						  (:button-release (apply #'handle-button-release event-slots))
						  (:motion-notify (apply #'handle-motion-notify event-slots))
						  ((or :exposure) (apply #'handle-exposure event-slots)))
						t)
			   :timeout *gui-update*)
       (xlib:display-force-output *display*)
       (do-repl)))
  (tx-close)
  (xlib:close-display *display*))




