(in-package :line-synth)

(define-main-key ("q" :alt) 'done)
(define-main-key ("x" :alt) 'done)

(define-main-key ("space") 'cmd-toggle-audio)

(define-main-key ("space" :control) 'key-place-selection-mark)
(define-main-key ("space" :alt) 'hide-selection)


(define-main-key ("Return") 'toggle-current-line-lock)


(define-main-key ("c") 'center-on-current-line)

(define-main-key ("s") 'toggle-window-synchro)
(define-main-key ("s" :control) 'toggle-all-window-synchro)

(define-main-key ("s" :shift) 'synchronize-other-from-current)
(define-main-key ("s" :shift :control) 'synchronize-other-from-current+value)

(define-main-key ("f") 'toggle-window-follow-cursor)
(define-main-key ("f" :control) 'toggle-all-follow-cursor)

(define-main-key ("n") 'add-window)
(define-main-key ("x") 'remove-window)

(define-main-key ("r") 'reorder-current-line-points)

(define-main-key ("z") 'zoom-on-selection)

(define-main-key ("Right") 'display-to-right)
(define-main-key ("Left") 'display-to-left)
(define-main-key ("Up") 'display-to-up)
(define-main-key ("Down") 'display-to-down)

(define-main-key ("Tab") 'center-on-cursor)



(define-main-key ("Page_Up") 'key-zoom-in-time)
(define-main-key ("Page_Down") 'key-zoom-out-time)
(define-main-key ("Page_Up" :control) 'key-zoom-in-value)
(define-main-key ("Page_Down" :control) 'key-zoom-out-value)


(define-main-key ("q") 'key-set-zero-current-point)
(define-main-key ("a") 'key-set-max-current-point)
(define-main-key ("w") 'key-set-min-current-point)

(define-main-key ("x" :control) 'cut-selection)
(define-main-key ("c" :control) 'copy-selection)
(define-main-key ("v" :control) 'paste-selection)


(define-main-key ("d") 'detach-current-line)
(define-main-key ("d" :shift) 'attach-current-line)


(define-mouse (1) 'handle-mouse-1 'release-point)
(define-mouse (2) 'del-point-on-window)
(define-mouse (3) 'handle-mouse-3)

(define-mouse (1 :control) 'set-zero-current-point)
(define-mouse (1 :control :shift) 'set-max-current-point)
(define-mouse (3 :control :shift) 'set-min-current-point)

(define-mouse (4) 'zoom-in-time)
(define-mouse (5) 'zoom-out-time)

(define-mouse (4 :control) 'zoom-in-value)
(define-mouse (5 :control) 'zoom-out-value)

(define-mouse ('motion) 'move-point)

(define-mouse (1 :shift) 'place-selection-mark)
