(in-package :line-synth)

(defparameter *timer-origin* nil)
(defparameter *update-delay* 0.1)
(defparameter *delay-ratio* 10)
(defparameter *gui-update* (/ *update-delay* *delay-ratio*))
(defparameter *next-update* 0)


(declaim (inline realtime->s s->realtime delay-snap))

(defun realtime->s (rtime)
  (float (/ rtime internal-time-units-per-second)))

(defun s->realtime (second)
  (round (* second internal-time-units-per-second)))

(defparameter *update-delay-rt* (s->realtime *update-delay*))


(defun set-delay (delay)
  "Fixe the update delay"
  (setf *update-delay* delay
	*update-delay-rt* (s->realtime *update-delay*)
	*gui-update* (/ delay *delay-ratio*)))


(defun delay-snap (rtime)
  (* (ceiling (/ rtime *update-delay-rt*)) *update-delay-rt*))



(defun start-timer (&optional (t-start 0))
  (setf *audio-playing* t
	*timer-origin* (- (get-internal-real-time) (s->realtime t-start))
	*next-update* (delay-snap *timer-origin*)))

(defun stop-timer ()
  (setf *audio-playing* nil
	*timer-origin* nil))

(defun current-timer ()
  (if *audio-playing*
      (realtime->s (delay-snap (- (get-internal-real-time) *timer-origin*)))
      0))



(defun update-needed ()
  (when (>= (get-internal-real-time) *next-update*)
    (setf *next-update* (delay-snap (get-internal-real-time)))
    t))
