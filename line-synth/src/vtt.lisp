(in-package :line-synth)



(defun vtt-effect-list (vtt)
  (let (acc)
    (dolist (line (vtt-lines-list vtt))
      (pushnew (effect-id (line-effect line)) acc))
    acc))


(defun draw-vtt (vtt)
  (dolist (line (vtt-lines-list vtt))
    (draw-line line)))

(defun draw-vtt-xwindow (vtt xwindow)
  (dolist (line (vtt-lines-list vtt))
    (when (equal xwindow (window-xwindow (line-window line)))
      (draw-line line))))


(defun update-vtt ()
  (dolist (vtt *vtt-list*)
    (dolist (line (vtt-lines-list vtt))
      (funcall (effect-fun (line-effect line)) (effect-param (line-effect line)) (line-point line (current-timer))))))


(defun set-current-vtt (name)
  (setf *current-vtt* nil
	*current-line* nil)
  (let ((sname (to-string name)))
    (dolist (vtt *vtt-list*)
      (when (string-equal sname (vtt-name vtt))
	(setf *current-vtt* vtt
	      *current-line* nil))))
  (need-redisplay)
  nil)


(defun set-current-line (name)
  (setf *current-line* nil)
  (let ((sname (to-string name)))
    (when *current-vtt*
      (dolist (line (vtt-lines-list *current-vtt*))
	(when (string-equal sname (effect-name (line-effect line)))
	  (setf *current-line* line)))))
  (need-redisplay)
  nil)





(defun default-line (id vtt effect-fun window)
  (let* ((effect (find-effect-by-fun effect-fun)))
    (setf (effect-param effect) (vtt-tx-vtt vtt))
    (create-line id vtt effect window)))


(defun add-builtin-effect-from-list (effect-fun)
  (let ((window (add-window))
	(id -1))
    (setf (vtt-lines-list *current-vtt*)
	  (append (vtt-lines-list *current-vtt*)
		  (loop for fun in effect-fun
		     collect (default-line (incf id) *current-vtt* fun window))))
    (need-redisplay)))


(defun add-builtin-default-effect ()
  (add-builtin-effect-from-list (list #'tx-set-volume #'tx-set-pan #'tx-set-pitch)))

(defun add-builtin-lowpass-filter ()
  (add-builtin-effect-from-list (list #'tx-set-lp-enable #'tx-set-lp-gain #'tx-set-lp-reso #'tx-set-lp-freq)))

(defun add-builtin-echo ()
  (add-builtin-effect-from-list (list #'tx-set-ec-enable #'tx-set-ec-length #'tx-set-ec-feedback #'tx-set-ec-volume #'tx-set-ec-pan)))


(defun add-ladspa-effect (effect-num)
  (when (add-vtt-fx (vtt-tx-vtt *current-vtt*) effect-num)
    (let ((window (add-window))
	  (id -1))
      (setf (vtt-lines-list *current-vtt*)
	    (append (vtt-lines-list *current-vtt*)
		    (loop for control = (get-next-fx-control)
		       while (not (null-pointer-p control))
		       collect (let ((effect (make-effect :id effect-num
							  :name (get-fx-control-name control)
							  :label-name (extract-fx-control-label-name control)
							  :fun #'set-fx-control-value
							  :param control
							  :default-value (get-fx-control-min-value control)
							  :min (get-fx-control-min-value control)
							  :max (get-fx-control-max-value control)
							  :color (color))))
				 (push effect *effect-list*)
				 (create-line (incf id) *current-vtt* effect window)))))
      (need-redisplay))))




(defun add-fx-on-current-vtt (effect-num)
  (labels ((equal-effect-id (fun)
	     (equal effect-num (effect-id (find-effect-by-fun fun)))))
    (unless (warn-no-current-vtt)
      (cond ((equal-effect-id  #'tx-set-ec-enable) (add-builtin-echo))
	    ((equal-effect-id  #'tx-set-lp-enable) (add-builtin-lowpass-filter))
	    ((equal-effect-id  #'tx-set-volume) (add-builtin-default-effect))
	    (t (add-ladspa-effect effect-num))))))


(defun remove-current-fx-on-current-vtt ()
  (unless (warn-no-current-line)
    (setf (vtt-lines-list *current-vtt*)
	  (remove-if (lambda (x)
		       (equal (effect-id (line-effect x)) (effect-id (line-effect *current-line*))))
		     (vtt-lines-list *current-vtt*)))
    (need-redisplay)))


(defun next-vtt-id ()
  (find-free-number (loop for vtt in *vtt-list*
		       collect (vtt-id vtt))))


(defun add-vtt (vtt-name &optional (create-default t) color id)
  (let* ((name (to-string vtt-name)))
    (when (probe-file name)
      (setf *current-vtt* (make-vtt :id (or id (next-vtt-id))
				    :name name :color (or color "magenta") :tx-vtt (tx-add-vtt name)))
      (when create-default
	(add-builtin-default-effect))
      ;;(add-builtin-lowpass-filter)
      ;;(add-builtin-echo)
      ;;(add-ladspa-effect 1185)
      (push *current-vtt* *vtt-list*)
      *current-vtt*)))


