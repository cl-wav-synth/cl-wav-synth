(in-package :cl-user)


(defpackage line-synth
  (:use :common-lisp :cffi)
  (:export :test :run))


(in-package :line-synth)

(defparameter *display* nil)
(defparameter *screen* nil)
(defparameter *root* nil)
(defparameter *default-font* nil)
(defparameter *default-font-string* "fixed")

(defparameter *max-char-width* nil)
(defparameter *max-char-height* nil)

(defparameter *select-dist* 10)
(defparameter *move-zone* 30)

(defparameter *window-event-mask* '(:key-press :key-release :button-press :button-release
				    :pointer-motion
				    :exposure :property-change :structure-notify))

(defparameter *vtt-list* nil)
(defparameter *window-list* nil)
(defparameter *effect-list* nil)

(defparameter *current-vtt* nil)

(defparameter *current-line* nil)
(defparameter *current-line-lock* nil)

(defparameter *in-move-current-line* nil)

(defparameter *need-redisplay* nil)

(defparameter *zoom-sensibility* 0.3)

(defparameter *pointer-x* 0)
(defparameter *pointer-y* 0)
(defparameter *pointer-xwindow* nil)

(defparameter *audio-playing* nil)

(defstruct lp time val)

(defstruct effect id fun param name label-name color default-value min max)

(defstruct line id vtt effect window color vmin vmax points t1 t2 v1 v2 a sel-x1 sel-x2)

(defstruct window xwindow xgc tmin tmax vmin vmax (tcursor 0) posx posy pointer-text (follow-cursor t) (sync t))

(defstruct vtt id name color tx-vtt lines-list)

(defstruct sel window mark1 mark2 lines)


(defparameter *selection* nil)
(defparameter *sel-first-mark* t)





