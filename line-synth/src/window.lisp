(in-package :line-synth)

(defun win-time->pos (time window)
  (time->pos time (window-tmin window) (window-tmax window)
	     (xlib:drawable-width (window-xwindow window))))

(defun win-time->ipos (time window)
  (time->ipos time (window-tmin window) (window-tmax window)
	     (xlib:drawable-width (window-xwindow window))))

(defun win-pos->time (pos window)
  (pos->time pos (window-tmin window) (window-tmax window)
	     (xlib:drawable-width (window-xwindow window))))

(defun win-val->pos (val window)
  (val->pos val (window-vmin window) (window-vmax window)
	    (xlib:drawable-height (window-xwindow window))))

(defun win-val->ipos (val window)
  (val->ipos val (window-vmin window) (window-vmax window)
	     (xlib:drawable-height (window-xwindow window))))

(defun win-pos->val (pos window)
  (pos->val pos (window-vmin window) (window-vmax window)
	    (xlib:drawable-height (window-xwindow window))))




(defun add-window (&optional tmin tmax vmin vmax)
  "Add a new window"
  (let* ((xwindow (xlib:create-window :parent *root*
				      :x 100 :y 100 :width 800 :height 600
				      :background (get-color "black")
				      :colormap (xlib:screen-default-colormap *screen*)
				      :event-mask *window-event-mask*))
	 (gcontext (xlib:create-gcontext :drawable xwindow
					 :background (get-color "black")
					 :foreground (get-color "white")
					 :font *default-font*))
	 (window (make-window :xwindow xwindow :xgc gcontext
			      :tmin (or tmin -1) :tmax (or tmax 60)
			      :vmin (or vmin -10) :vmax (or vmax 10))))
    (xlib:map-window xwindow)
    (xlib:display-finish-output *display*)
    (push window *window-list*)
    window))

(defun delete-window (window)
  "Delete a window"
  (xlib:free-gcontext (window-xgc window))
  (xlib:destroy-window (window-xwindow window))
  (setf *window-list* (remove window *window-list*))
  (xlib:display-finish-output *display*))


(defun empty-window-p (window)
  (let ((empty t))
    (dolist (vtt *vtt-list*)
      (dolist (line (vtt-lines-list vtt))
	(when (equal (line-window line) window)
	  (setf empty nil))))
    empty))

(defun remove-window ()
  "Remove the current window if empty"
  (multiple-value-bind (x y)
      (xlib:global-pointer-position *display*)
    (dolist (win *window-list*)
      (let ((xwin (window-xwindow win)))
	(when (and (< (xlib:drawable-x xwin)
		      x
		      (+ (xlib:drawable-x xwin) (xlib:drawable-width xwin)))
		 (< (xlib:drawable-y xwin)
		    y
		    (+ (xlib:drawable-y xwin) (xlib:drawable-height xwin))))
	  (let ((win (find-window xwin)))
	    (if (empty-window-p win)
		(delete-window win)
		(progn
		  (format t "This window is not empty. Remove all lines if you want to remove it.~%")
		  (display-prompt "> ")))))))))



(defun draw-axis (window)
  (let ((tmin (window-tmin window))
	(tmax (window-tmax window))
	(vmin (window-vmin window))
	(vmax (window-vmax window))
	(width (xlib:drawable-width (window-xwindow window)))
	(height (xlib:drawable-height (window-xwindow window))))
    (set-gcontext window "white" "black" :double-dash boole-1)
    (when (< vmin 0 vmax)
      (xlib:draw-line (window-xwindow window) (window-xgc window)
		      0 (val->ipos 0 vmin vmax height)
		      width (val->ipos 0 vmin vmax height)))
    (when (< tmin 0 tmax)
      (xlib:draw-line (window-xwindow window) (window-xgc window)
		      (time->ipos 0 tmin tmax width) 0
		      (time->ipos 0 tmin tmax width) height))
    (when *in-move-current-line*
      (let ((text (format nil "Where do you want to move ~A on ~A?"
			  (effect-name (line-effect *current-line*))
			  (vtt-name (line-vtt *current-line*)))))
	(draw-xy-text window text
		      (round (/ (- width (* (length text) *max-char-width*)) 2))
		      (- height *max-char-height*)
		      "red")))
    (when (or *current-line-lock* (window-follow-cursor window) (window-sync window))
      (draw-xy-text window (format nil "~A ~A ~A"
				   (if *current-line-lock* "Lock" "")
				   (if (window-follow-cursor window) "Follow" "")
				   (if (window-sync window) "Synchro" ""))
		    (round (/ (- width (* 6 *max-char-width*)) 2))
		    (- height (xlib:font-descent *default-font*)) "green"))
    (xlib:display-finish-output *display*)))



(defun draw-selection ()
  (let ((window (sel-window *selection*)))
    (when window
      (let* ((height (xlib:drawable-height (window-xwindow window)))
	     (mark1 (aif (sel-mark1 *selection*) (win-time->ipos it window) nil))
	     (mark2 (aif (sel-mark2 *selection*) (win-time->ipos it window) nil))
	     (mark-min (min (or mark1 0) (or mark2 0)))
	     (mark-max (max (or mark1 0) (or mark2 0))))
	(when (and mark1 mark2)
	  (set-gcontext window "blue" "blue" :solid boole-xor)
	  (xlib:draw-rectangle (window-xwindow window) (window-xgc window)
			       mark-min
			       0
			       (- mark-max mark-min)
			       height
			       t))
	(when (or mark1 mark2)
	  (set-gcontext window "green" "green" :solid boole-xor)
	  (xlib:draw-line (window-xwindow window) (window-xgc window)
			  (if *sel-first-mark* mark1 mark2)
			  0
			  (if *sel-first-mark* mark1 mark2)
			  height))
	(xlib:display-finish-output *display*)))))






(defun draw-cursor (window &optional (clear t))
  (let ((tmin (window-tmin window))
	(tmax (window-tmax window))
	(width (xlib:drawable-width (window-xwindow window)))
	(height (xlib:drawable-height (window-xwindow window))))
    (labels ((draw (time)
	       (when (< tmin time tmax)
		 (xlib:draw-line (window-xwindow window) (window-xgc window)
				 (time->ipos time tmin tmax width) 0
				 (time->ipos time tmin tmax width) height))
	       (when (window-pointer-text window)
		 (xlib:draw-glyphs (window-xwindow window) (window-xgc window)
				   0 (- height (xlib:font-descent *default-font*))
				   (window-pointer-text window)))))
      (set-gcontext window "white" "black" :solid boole-xor)
      (when clear
	(draw (window-tcursor window)))
      (setf (window-tcursor window) (current-timer)
	    (window-pointer-text window) (if (equal (window-xwindow window)
						    *pointer-xwindow*)
					     (format nil "t=~A v=~,3F ~A"
						     (sec->hms-string (win-pos->time *pointer-x* window))
						     (win-pos->val *pointer-y* window)
						     (aif (find-line-from-name window *pointer-x* *pointer-y*)
							  (format nil " | ~A - ~A (~A,~A,~A)"
								  (vtt-name (line-vtt it))
								  (effect-name (line-effect it))
								  (vtt-id (line-vtt it))
								  (effect-id (line-effect it))
								  (line-id it))
							  ""))
					     "---"))
      (draw (window-tcursor window)))
    (xlib:display-finish-output *display*)))


(defun synchro-time-from-cursor (window)
  (when (and *audio-playing* (window-follow-cursor window))
    (let ((width (- (window-tmax window) (window-tmin window))))
      (unless (<= (+ (window-tmin window) (* width 1/10))
		  (window-tcursor window)
		  (- (window-tmax window) (* width 1/10)))
	(setf (window-tmin window) (- (window-tcursor window) (* width 1/10))
	      (window-tmax window) (+ (window-tcursor window) (* width 9/10)))
	(need-redisplay)))))

(defun update-cursors ()
  (dolist (window *window-list*)
    (synchro-time-from-cursor window)
    (draw-cursor window)))



(defun zoom-out-time (xwindow x y)
  "Zoom out on the time axis"
  (declare (ignore x y))
  (let ((window (find-window xwindow)))
    (when window
      (let ((width/2 (/ (- (window-tmax window) (window-tmin window)) 2)))
	(decf (window-tmin window) width/2)
	(incf (window-tmax window) width/2))
      (need-redisplay)
      (synchronize-other-from-window window))))

(defun zoom-in-time (xwindow x y)
  "Zoom in on the time axis"
  (let ((window (find-window xwindow)))
    (when window
      (let* ((decal (float (/ x (xlib:drawable-width xwindow))))
	     (dtime (* (- (window-tmax window) (window-tmin window)) (- decal 0.5)))
	     (width/2 (/ (- (window-tmax window) (window-tmin window)) 2)))
	(incf (window-tmin window) (+ dtime (* width/2 *zoom-sensibility*)))
	(incf (window-tmax window) (- dtime (* width/2 *zoom-sensibility*)))
	(xlib:warp-pointer xwindow (truncate (/ (xlib:drawable-width xwindow) 2)) y))
      (need-redisplay)
      (synchronize-other-from-window window))))



(defun zoom-out-value (xwindow x y)
  "Zoom out on the value axis"
  (declare (ignore x y))
  (let ((window (find-window xwindow)))
    (when window
      (let ((height/2 (/ (- (window-vmax window) (window-vmin window)) 2)))
	(decf (window-vmin window) height/2)
	(incf (window-vmax window) height/2))
      (need-redisplay)
      (synchronize-other-from-window window))))


(defun zoom-in-value (xwindow x y)
  "Zoom out in the value axis"
  (let ((window (find-window xwindow)))
    (when window
      (let* ((decal (float (/ y (xlib:drawable-height xwindow))))
	     (dvalue (* (- (window-vmin window) (window-vmax window)) (- decal 0.5)))
	     (height/2 (/ (- (window-vmax window) (window-vmin window)) 2)))
	(incf (window-vmin window) (+ dvalue (* height/2 *zoom-sensibility*)))
	(incf (window-vmax window) (- dvalue (* height/2 *zoom-sensibility*)))
	(xlib:warp-pointer xwindow x (truncate (/ (xlib:drawable-height xwindow) 2))))
      (need-redisplay)
      (synchronize-other-from-window window))))






(defmacro def-key-equiv (symbol description)
  (let ((fun (intern (string-upcase (format nil "key-~A" symbol)))))
    `(defun ,fun ()
       ,description
       (multiple-value-bind (xwindow x y)
	   (key-query-pointer)
	 (,symbol xwindow x y)))))


(def-key-equiv zoom-in-time
    "Zoom in on the time axis")
(def-key-equiv zoom-out-time
    "Zoom out on the time axis")

(def-key-equiv zoom-in-value
    "Zoom in on the value axis")
(def-key-equiv zoom-out-value
    "Zoom out on the value axis")



(defun display-to-horiz-dir (fun)
  (multiple-value-bind (xwindow x y)
      (key-query-pointer)
    (declare (ignore x y))
    (let ((window (find-window xwindow)))
      (when window
	(let ((width/2 (/ (- (window-tmax window) (window-tmin window)) 2)))
	  (setf (window-tmin window) (funcall fun (window-tmin window) width/2)
		(window-tmax window) (funcall fun (window-tmax window) width/2)))
	(need-redisplay)
	(synchronize-other-from-window window)))))

(defun display-to-right ()
  "Display the next half right screen"
  (display-to-horiz-dir #'+))
(defun display-to-left ()
  "Display the next half left screen"
  (display-to-horiz-dir #'-))


(defun display-to-vert-dir (fun)
  (multiple-value-bind (xwindow x y)
      (key-query-pointer)
    (declare (ignore x y))
    (let ((window (find-window xwindow)))
      (when window
	(let ((height/2 (/ (- (window-vmax window) (window-vmin window)) 2)))
	  (setf (window-vmin window) (funcall fun (window-vmin window) height/2)
		(window-vmax window) (funcall fun (window-vmax window) height/2)))
	(need-redisplay)
	(synchronize-other-from-window window)))))

(defun display-to-up ()
  "Display the next half up screen"
  (display-to-vert-dir #'+))
(defun display-to-down ()
  "Display the next half down screen"
  (display-to-vert-dir #'-))


(defun center-on-cursor ()
  "Center on the cursor position"
  (multiple-value-bind (xwindow x y)
      (key-query-pointer)
    (declare (ignore x y))
    (let ((window (find-window xwindow)))
      (when window
	(let ((width/2 (/ (- (window-tmax window) (window-tmin window)) 2)))
	  (setf (window-tmin window) (- (window-tcursor window) width/2)
		(window-tmax window) (+ (window-tcursor window) width/2)))
	(need-redisplay)
	(synchronize-other-from-window window)))))

