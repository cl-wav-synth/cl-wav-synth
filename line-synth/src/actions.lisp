(in-package :line-synth)

(defun find-vtt-by-id (vtt-id)
  (dolist (vtt *vtt-list*)
    (when (= (vtt-id vtt) vtt-id)
      (return-from find-vtt-by-id vtt))))

(defun find-line-by-id (vtt-id effect-id line-id)
  (let ((vtt (find-vtt-by-id vtt-id)))
    (when vtt
      (dolist (line (vtt-lines-list vtt))
	(when (and (= (effect-id (line-effect line)) effect-id)
		   (= (line-id line) line-id))
	  (return-from find-line-by-id (values vtt line))))))
  (values nil nil))



(defun select-vtt-by-id (vtt-id)
  (setf *current-vtt* (find-vtt-by-id vtt-id)
	*current-line* nil
	*current-line-lock* nil)
  (when *current-vtt*
    (need-redisplay)))

(defun select-line-by-id (vtt-id effect-id line-id)
  (multiple-value-bind (vtt line)
      (find-line-by-id vtt-id effect-id line-id)
    (setf *current-vtt* vtt
	  *current-line* line
	  *current-line-lock* t)
    (when vtt
      (need-redisplay))))

(defun select-line-current-vtt (effect-id line-id)
  (select-line-by-id (vtt-id *current-vtt*) effect-id line-id))


(defun reorder-current-line-points ()
  "Reorder current line points"
  (unless (warn-no-current-line)
    (reorder-line-points *current-line*)
    (need-redisplay)))


(defun center-on-current-line ()
  "Center on the current line"
  (unless (warn-no-current-line)
    (let ((decal (/ (- (line-vmax *current-line*) (line-vmin *current-line*)) 20)))
      (setf (window-vmin (line-window *current-line*)) (- (line-vmin *current-line*) decal)
	    (window-vmax (line-window *current-line*)) (+ (line-vmax *current-line*) decal)))
    (need-redisplay)))

(defun synchronize-other-from-current ()
  "Synchronize other windows time from the current window"
  (let ((orig (find-window (key-query-pointer))))
    (when orig
      (dolist (win *window-list*)
	(setf (window-tmin win) (window-tmin orig)
	      (window-tmax win) (window-tmax orig))))
    (need-redisplay)))

(defun synchronize-other-from-current+value ()
  "Synchronize other windows time and value from the current window"
  (let ((orig (find-window (key-query-pointer))))
    (when orig
      (dolist (win *window-list*)
	(setf (window-tmin win) (window-tmin orig)
	      (window-tmax win) (window-tmax orig)
	      (window-vmin win) (window-vmin orig)
	      (window-vmax win) (window-vmax orig))))
    (need-redisplay)))


(defun synchronize-other-from-window (window)
  "Synchronize other windows time and value from the given window"
  (dolist (win *window-list*)
    (when (window-sync win)
      (setf (window-tmin win) (window-tmin window)
	    (window-tmax win) (window-tmax window)
	    (window-vmin win) (window-vmin window)
	    (window-vmax win) (window-vmax window))))
  (need-redisplay))




(defun init-current-line (&optional tmin tmax vmin vmax)
  (setf (line-points *current-line*) nil
	(window-tmin (line-window *current-line*)) tmin
	(window-tmax (line-window *current-line*)) tmax
	(window-vmin (line-window *current-line*)) vmin
	(window-vmax (line-window *current-line*)) vmax))


(defun clear-all ()
  (tx-remove-all-vtt)
  (setf *vtt-list* nil)
  (dolist (win *window-list*)
    (delete-window win))
  (setf *window-list* nil))




;;; Save/Load code
(defun save-state (filename)
  (with-open-file (stream (to-string filename) :direction :output
			  :if-exists :supersede
			  :if-does-not-exist :create)
    (labels ((-> (&rest args)
	       (dolist (a args)
		 (format stream "~S~%" a)))
	     (newline ()
	       (format stream "~%")))
      (format stream ";;; -*- lisp -*-~2%")
      (dolist (vtt *vtt-list*)
	(format stream ";;; VTT: ~A~%" (vtt-name vtt))
	(-> `(add-vtt ,(vtt-name vtt) nil))
	(dolist (effect-num (vtt-effect-list vtt))
	  (-> `(add-fx-on-current-vtt ,effect-num)))
	(newline)
	(dolist (line (vtt-lines-list vtt))
	  (format stream ";;; VTT: ~A  Effect: ~A~%"  (vtt-name vtt) (effect-name (line-effect line)))
	  (-> `(select-line-current-vtt ,(effect-id (line-effect line)) ,(line-id line)))
	  (-> `(init-current-line
		,(window-tmin (line-window line)) ,(window-tmax (line-window line))
		,(window-vmin (line-window line)) ,(window-vmax (line-window line))))
	  (dolist (pt (line-points line))
	    (-> `(add-point ,(lp-time pt) ,(lp-val pt))))
	  (newline))
	(newline)))))



(defun load-state (filename)
  (load (to-string filename)))



;;; Triangles functions
(defun gen-triangle-on-current-line (t-start t-period n-period min max duty)
  (unless (warn-no-current-line)
    (dotimes (i n-period)
      (add-point (+ t-start (* i t-period)) min)
      (add-point (+ t-start (* (+ i duty) t-period)) max))
    (reorder-current-line-points)
    (need-redisplay)))

(defun triangle-on-current-line (t-start t-period n-period min max)
  (gen-triangle-on-current-line t-start t-period n-period min max 1))

(defun sym-triangle-on-current-line (t-start t-period n-period min max)
  (gen-triangle-on-current-line t-start t-period n-period min max 0.5))


;;; Square functions
(defun gen-square-on-current-line (t-start t-period n-period min max duty)
  (unless (warn-no-current-line)
    (dotimes (i n-period)
      (add-point (+ t-start (* i t-period)) min)
      (add-point (+ t-start (* (+ i duty) t-period)) min)
      (add-point (+ t-start (* (+ i duty) t-period)) max)
      (add-point (+ t-start (* (1+ i) t-period)) max))
    (reorder-current-line-points)
    (need-redisplay)))

(defun square-on-current-line (t-start t-period n-period min max)
  (gen-square-on-current-line t-start t-period n-period min max 0.5))


;;; Sinus function
(defun sinus-on-current-line (t-start t-period n-period min max prec)
  (unless (warn-no-current-line)
    (let ((freq (/ t-period))
	  (delta (/ (- max min) 2))
	  (moy (/ (+ max min) 2)))
      (dotimes (period n-period)
	(dotimes (pts prec)
	  (add-point (float (+ t-start (* pts t-period (/ prec)) (* period t-period)))
		     (float (+ moy (* delta (sin (* 2 pi freq (+ (* pts t-period (/ prec)))))))))))
      (let ((pts prec)
	    (period (1- n-period)))
	(add-point (float (+ t-start (* pts t-period (/ prec)) (* period t-period)))
		   (float (+ moy (* delta (sin (* 2 pi freq (+ (* pts t-period (/ prec)))))))))))
    (reorder-current-line-points)
    (need-redisplay)))



;;; Copy function
(defun copy-line-by-id (vtt-id effect-id line-id)
  (unless (warn-no-current-line)
    (multiple-value-bind (vtt line)
	(find-line-by-id vtt-id effect-id line-id)
      (declare (ignore vtt))
      (when line
	(setf (line-points *current-line*)
	      (loop for pt in (line-points line)
		 collect (copy-lp pt)))
	(reorder-current-line-points)
	(need-redisplay)))))


(defun copy-line-by-id-time (vtt-id effect-id line-id t-start t-end)
  (unless (warn-no-current-line)
    (multiple-value-bind (vtt line)
	(find-line-by-id vtt-id effect-id line-id)
      (declare (ignore vtt))
      (when line
	(setf (line-points *current-line*)
	      (append
	       (loop for pt in (line-points *current-line*)
		  when (<= (lp-time pt) t-start)
		  collect (copy-lp pt))
	       (list (make-lp :time t-start :val (line-point line t-start)))
	       (loop for pt in (line-points line)
		  when (<= t-start (lp-time pt) t-end)
		  collect (copy-lp pt))
	       (list (make-lp :time t-end :val (line-point line t-end)))
	       (loop for pt in (line-points *current-line*)
		  when (<= t-end (lp-time pt))
		  collect (copy-lp pt))))
	(reorder-current-line-points)
	(need-redisplay)))))



(defun repeat-points (t-start t-end t-begin n-repeat t-space)
  (unless (or (warn-no-current-line)
	      (<= t-start t-begin t-end))
    (dotimes (n n-repeat)
      (setf (line-points *current-line*)
	    (append
	     (line-points *current-line*)
	     (list (make-lp :time (+ t-begin (* n t-space)) :val (line-point *current-line* t-start)))
	     (loop for pt in (line-points *current-line*)
		when (<= t-start (lp-time pt) t-end)
		collect (make-lp :time (+ t-begin (- (lp-time pt) t-start) (* n t-space))
				 :val (lp-val pt)))
	     (list (make-lp :time (+ t-begin (- t-end t-start) (* n t-space))
			    :val (line-point *current-line* t-start))))))
    (reorder-current-line-points)
    (need-redisplay)))



;;; Do macro
(defmacro do-lp (&body body)
  `(unless (warn-no-current-line)
     (dolist (it (line-points *current-line*))
       (symbol-macrolet ((time (lp-time it))
			 (val (lp-val it)))
	 ,@body))
     (reorder-current-line-points)
     (need-redisplay)))

(defmacro do-lp-t ((t-start t-end) &body body)
  `(unless (warn-no-current-line)
     (dolist (it (line-points *current-line*))
       (when (<= ,t-start (lp-time it) ,t-end)
	 (symbol-macrolet ((time (lp-time it))
			   (val (lp-val it)))
	   ,@body)))
     (reorder-current-line-points)
     (need-redisplay)))

(defmacro do-lp-sel (&body body)
  `(do-lp-t ((selection-min *selection*)
	     (selection-max *selection*))
     ,@body))

(defmacro do-all-lp (&body body)
  `(progn
     (dolist (vtt *vtt-list*)
       (dolist (line (vtt-lines-list vtt))
	 (dolist (it (line-points line))
	   (symbol-macrolet ((time (lp-time it))
			     (val (lp-val it)))
	     ,@body)))
       (reorder-current-line-points)
       (need-redisplay))))




(defun toggle-all-follow-cursor ()
  "Toggle all window follow cursor"
  (dolist (win *window-list*)
    (setf (window-follow-cursor win) (not (window-follow-cursor win))))
  (need-redisplay))

(defun toggle-window-follow-cursor ()
  "Toogle window follow cursor"
  (let ((win (find-window (key-query-pointer))))
    (when win
      (setf (window-follow-cursor win) (not (window-follow-cursor win)))
      (need-redisplay))))


(defun toggle-all-window-synchro ()
  "Toggle all window synchronisation"
  (dolist (win *window-list*)
    (setf (window-sync win) (not (window-sync win))))
  (need-redisplay))

(defun toggle-window-synchro ()
  "Toogle window synchronisation"
  (let ((win (find-window (key-query-pointer))))
    (when win
      (setf (window-sync win) (not (window-sync win)))
      (need-redisplay))))

(defun place-selection-mark (xwindow x y)
  "Place a selection marker"
  (declare (ignore y))
  (let ((win (find-window xwindow)))
    (when win
      (setf (sel-window *selection*) win
	    *sel-first-mark* (not *sel-first-mark*))
      (if *sel-first-mark*
	  (setf (sel-mark1 *selection*) (win-pos->time x win))
	  (setf (sel-mark2 *selection*) (win-pos->time x win))))
    (need-redisplay)))



(defun key-place-selection-mark ()
  "Place a selection marker"
  (multiple-value-bind (xwindow x y)
      (key-query-pointer)
    (place-selection-mark xwindow x y)))

(let ((old-window nil))
  (defun hide-selection ()
    "Hide the selection"
    (if old-window
	(setf (sel-window *selection*) old-window
	      old-window nil)
	(setf old-window (sel-window *selection*)
	      (sel-window *selection*) nil))
    (need-redisplay)))



(defun copy-selection ()
  "Copy the selection"
  (when (sel-window *selection*)
    (let ((mark-min (selection-min *selection* nil))
	  (mark-max (selection-max *selection* nil)))
      (when (and mark-min mark-max)
	(setf (sel-lines *selection*) nil)
	(dolist (vtt *vtt-list*)
	  (dolist (line (vtt-lines-list vtt))
	    (when (equal (line-window line) (sel-window *selection*))
	      (push (make-line :id (line-id line) :effect (line-effect line)
			       :points (append (list (make-lp :time 0 :val (line-point line mark-min)))
					       (loop for pt in (line-points line)
						  when (<= mark-min (lp-time pt) mark-max)
						  collect (make-lp :time (- (lp-time pt) mark-min)
								   :val (lp-val pt)))
					       (list (make-lp :time (- mark-max mark-min) :val (line-point line mark-max)))))
		    (sel-lines *selection*)))))))))


(defun cut-selection ()
  "Cut the selection"
  (copy-selection)
  (when (sel-window *selection*)
    (let ((mark-min (selection-min *selection* nil))
	  (mark-max (selection-max *selection* nil)))
      (when (and mark-min mark-max)
	(let ((dec-time (- mark-max mark-min)))
	  (dolist (vtt *vtt-list*)
	    (dolist (line (vtt-lines-list vtt))
	      (when (equal (line-window line) (sel-window *selection*))
		(setf (line-points line) (remove-if #'(lambda (lp)
							(<= mark-min (lp-time lp) mark-max))
						    (line-points line))))
	      (dolist (lp (line-points line))
		(when (>= (lp-time lp) mark-max)
		  (decf (lp-time lp) dec-time)))))
	  (setf (sel-mark1 *selection*) mark-min
		(sel-mark2 *selection*) mark-min))
	(need-redisplay)))))


(defun paste-selection ()
  "Paste the selection"
  (multiple-value-bind (xwindow x)
      (key-query-pointer)
    (let* ((win (find-window xwindow))
	   (t-start (win-pos->time x win)))
      (when win
	(dolist (vtt *vtt-list*)
	  (dolist (line (vtt-lines-list vtt))
	    (when (equal (line-window line) win)
	      (dolist (sel-line (sel-lines *selection*))
		(when (and (equal (line-id line) (line-id sel-line))
			   (equal (effect-id (line-effect line)) (effect-id (line-effect sel-line))))
		  (let ((dec-time (lp-time (first (last (line-points sel-line))))))
		    (dolist (lp (line-points line))
		      (when (>= (lp-time lp) t-start)
			(incf (lp-time lp) dec-time)))
		    (setf (sel-window *selection*) win
			  (sel-mark1 *selection*) t-start
			  (sel-mark2 *selection*) (+ t-start dec-time)))
		  (dolist (lp (line-points sel-line))
		    (push (make-lp :time (+ t-start (lp-time lp))
				   :val (lp-val lp))
			  (line-points line)))
		  (reorder-line-points line))))))
	(need-redisplay)))))



(defun detach-current-line ()
  "Detach the current line and put it on its own window"
  (unless (warn-no-current-line)
    (let* ((win (line-window *current-line*))
	   (new-win (add-window (window-tmin win) (window-tmax win)
				(window-vmin win) (window-vmax win))))
      (setf (line-window *current-line*) new-win))
    (need-redisplay)))


(defun attach-current-line ()
  "Attach the current line"
  (unless (warn-no-current-line)
    (let* ((win (line-window *current-line*))
	   (found nil))
      (dolist (vtt *vtt-list*)
	(dolist (line (vtt-lines-list vtt))
	  (when (and (not (equal (line-window line) win))
		     (equal (vtt-id (line-vtt line)) (vtt-id (line-vtt *current-line*)))
		     (equal (effect-id (line-vtt line)) (effect-id (line-vtt *current-line*))))
	    (setf (line-window *current-line*) (line-window line)
		  found t))))
      (when (and found (empty-window-p win))
	(delete-window win)))
    (need-redisplay)))



(defun zoom-on-selection ()
  "Zoom on the current selection"
  (let ((win (sel-window *selection*)))
    (when (and win (sel-mark1 *selection*) (sel-mark2 *selection*))
      (setf (window-tmin win) (selection-min *selection*)
	    (window-tmax win) (selection-max *selection*))
      (synchronize-other-from-window win)
      (need-redisplay))))


(defun key-set-zero-current-point ()
  "Set the current point value to zero."
  (multiple-value-bind (xwindow x y)
      (key-query-pointer)
    (set-zero-current-point xwindow x y)))

(defun key-set-max-current-point ()
  "Set the current point value to its maximum."
  (multiple-value-bind (xwindow x y)
      (key-query-pointer)
    (set-max-current-point xwindow x y)))

(defun key-set-min-current-point ()
  "Set the current point value to its minimum."
  (multiple-value-bind (xwindow x y)
      (key-query-pointer)
    (set-min-current-point xwindow x y)))