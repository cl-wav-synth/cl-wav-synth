(in-package :line-synth)


(defun select-line-by-name (xwindow x y)
  (when (is-in-name-p y)
    (multiple-value-bind (line vtt)
	(find-line-from-name (find-window xwindow) x y)
      (setf *current-line-lock* (not (equal line *current-line*))
	    *current-line* line
	    *current-vtt* vtt)
      (need-redisplay))))



(defun select-line (window x y)
  (when (or (null *current-line*)
    	    (and *current-line* (not (equal window (window-xwindow (line-window *current-line*))))))
    (setf *current-line-lock* nil))
  (unless *current-line-lock*
    (let ((acc nil)
	  (last-current-line *current-line*))
      (dolist (vtt *vtt-list*)
	(dolist (line (vtt-lines-list vtt))
	  (when (equal window (window-xwindow (line-window line)))
	    (let* ((time (win-pos->time x (line-window line)))
		   (val (line-point line time))
		   (posy (win-val->ipos val (line-window line))))
	      (when (< (abs (- posy y)) *select-dist*)
		(push line acc))))))
      (let ((current-line? (member *current-line* acc :test #'equal)))
	(setf *current-line* (if (and current-line? (second current-line?))
				 (second current-line?)
				 (first acc))
	      *current-vtt* (when *current-line* (line-vtt *current-line*))))
      (unless (equal *current-line* last-current-line)
	(need-redisplay)))))


(defun add-point-on-window (window x y)
  "Add a point on the current line"
  (unless (warn-no-current-line)
    (when (equal window (window-xwindow (line-window *current-line*)))
      (let* ((time (win-pos->time x (line-window *current-line*)))
	     (val (max (min (win-pos->val y (line-window *current-line*))
			    (effect-max (line-effect *current-line*)))
		       (effect-min (line-effect *current-line*)))))
	(add-point time val)))))


(defun del-point-on-window (window x y)
  "Remove a point on the current line"
  (unless (warn-no-current-line)
    (when (equal window (window-xwindow (line-window *current-line*)))
      (dolist (pt (line-points *current-line*))
	(let ((px (win-time->pos (lp-time pt) (line-window *current-line*)))
	      (py (win-val->pos (lp-val pt) (line-window *current-line*))))
	  (when (and (< (abs (- px x)) *select-dist*)
		       (< (abs (- py y)) *select-dist*))
	    (del-point (lp-time pt))))))))

(let ((current-point nil)
      (current-window nil))
  (defun find-current-point (window x y)
    (when (and *current-line*
	       (equal window (window-xwindow (line-window *current-line*))))
      (dolist (p (line-points *current-line*))
	(let ((xtime (win-time->pos (lp-time p) (line-window *current-line*)))
	      (yval (win-val->pos (lp-val p) (line-window *current-line*))))
	  (when (and (< (abs (- x xtime)) *select-dist*)
		     (< (abs (- y yval)) *select-dist*))
	    (setf current-point p
		  current-window (line-window *current-line*)))))))


  (defun activate-point (window x y)
    (find-current-point window x y))

  (defun release-point (window x y)
    "Release a moving point"
    (declare (ignore window))
    (when (and current-point current-window)
      (setf (lp-time current-point) (win-pos->time x current-window)
	    (lp-val current-point) (max (min (win-pos->val y current-window)
					     (effect-max (line-effect *current-line*)))
					(effect-min (line-effect *current-line*))))
      (setf current-point nil
	    current-window nil)
      (reorder-current-line-points)
      (need-redisplay)))

  (defun move-point (window x y)
    "Move the point under mouse"
    (setf *pointer-x* x
	  *pointer-y* y
	  *pointer-xwindow* window)
    (when (and current-point current-window)
      (draw-line *current-line* nil)
      (setf (lp-time current-point) (win-pos->time x current-window)
	    (lp-val current-point) (max (min (win-pos->val y current-window)
					     (effect-max (line-effect *current-line*)))
					(effect-min (line-effect *current-line*))))
      (draw-line *current-line* nil)))

  (defun set-current-point-value (window x y value)
    (find-current-point window x y)
    (when (and current-point current-window)
      (setf (lp-val current-point) (max (min value
					     (effect-max (line-effect *current-line*)))
					(effect-min (line-effect *current-line*)))
            current-point nil
	    current-window nil)
      (reorder-current-line-points)
      (need-redisplay)))


  (defun set-zero-current-point (window x y)
    "Set the current point value to zero."
    (set-current-point-value window x y 0))

  (defun set-max-current-point (window x y)
    "Set the current point value to its maximum."
    (set-current-point-value window x y most-positive-single-float))

  (defun set-min-current-point (window x y)
    "Set the current point value to its minimum."
    (set-current-point-value window x y most-negative-single-float)))

(defun attach/detach-line-from-name (xwindow x y)
  (when (is-in-name-p y)
    (let ((window (find-window xwindow))
	  (ret nil))
      (when window
	(dolist (vtt *vtt-list*)
	  (dolist (line (vtt-lines-list vtt))
	    (when (and (equal window (line-window line))
		       (<= (line-sel-x1 line) x (line-sel-x2 line)))
	      (setf *current-line-lock* (not (equal line *current-line*))
	      	    *current-line* line
		    *current-vtt* vtt
		    *in-move-current-line* t)
	      (need-redisplay)
	      (setf ret t)))))
      ret)))


(defun move-current-line-here (xwindow)
  (setf (line-window *current-line*) (find-window xwindow)
	*in-move-current-line* nil)
  (need-redisplay))

(defun select-and-activate (xwindow x y)
  (select-line-by-name xwindow x y)
  (select-line xwindow x y)
  (activate-point xwindow x y))

(defun handle-mouse-1 (xwindow x y)
  "Select line and move point"
  (if *in-move-current-line*
      (move-current-line-here xwindow)
      (cond ((< x *move-zone*) (display-to-left))
	    ((> x (- (xlib:drawable-width xwindow) *move-zone*)) (display-to-right))
	    (t (select-and-activate xwindow x y)))))


(defun handle-mouse-3 (xwindow x y)
  "Move point on line or detach line on name"
  (unless (attach/detach-line-from-name xwindow x y)
    (add-point-on-window xwindow x y)))



