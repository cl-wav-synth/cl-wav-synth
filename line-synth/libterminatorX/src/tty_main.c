#include <stdio.h>
#include "tty_lib.h"
#include "tX_global.h"


int main(int argc, char **argv)
{
  int i;

  printf("TTY_TerminatorX start here\n");

  printf("my_cpp_function=%d\n", my_cpp_function (10));

  init_terminatorX();

  printf("Starting main loop\n");

  printf("Adding a vtt\n");

  add_vtt("synth.wav");
  add_vtt("tb303bass01.mp3");

  //globals.audiodevice_type = ALSA;
  //printf("%s %d %d %d\n", globals.oss_device, globals.oss_buff_no, globals.oss_buff_size, globals.oss_samplerate);

  //set_rel_pitch(0, 10);

  audio_on();


  for(i=0;i<100;i++)
    {
      set_rel_pitch(0, i/10-5);
      set_rel_pitch(1, 1+i/10-5);
      usleep(10000);
    }

  audio_off();

  close_terminatorX();

  return 0;
}
