#include "config.h"
#include "tX_audiodevice.h"
#include "tX_mastergui.h"
#include "tty_lib.h"
#include "tX_engine.h"
#include "tX_ladspa.h"
#include "tX_ladspa_class.h"
#include "tX_global.h"
#include "tX_sequencer.h"
#include "tX_vtt.h"
#include "tX_capabilities.h"


static tX_engine *engine;
extern int rec_dont_care;


#ifdef USE_JACK
void jack_check()
{
  if ((!tX_jack_client::get_instance()) && (globals.audiodevice_type==JACK)) {
    tx_note("Couldn't connect to JACK server - JACK output not available.\n\nIf you want to use JACK, ensure the JACK daemon is running before you start terminatorX.", true);
  }
}
#endif // USE_JACK



int my_cpp_function(int x)
{
  printf("x=%d\n", x);

  return 2;
}



void init_terminatorX(void)
{
  printf("Init... ");

  set_global_defaults();
  load_globals();

  globals.alternate_rc = 0;
  globals.store_globals = 1;
  globals.startup_set = 0;


#ifdef USE_CAPABILITIES
  if (!geteuid()) {
    if (prctl(PR_SET_KEEPCAPS, 1, -1, -1, -1)) {
      tX_error("failed to keep capabilites.");
    }
    set_nice_capability(CAP_PERMITTED);
  }
#endif

  if ((!geteuid()) && (getuid() != geteuid())) {
    tX_msg("runnig suid-root - dropping root privileges.");

    int result=setuid(getuid());

    if (result) {
      tX_error("main() Panic: can't drop root privileges.");
      exit(2);
    }
  }

  /* No suidroot below this comment. */

#ifdef USE_CAPABILITIES
  set_nice_capability(CAP_EFFECTIVE);
#endif

  engine=tX_engine::get_instance();
  LADSPA_Class::init();
  LADSPA_Plugin::init();

#ifdef USE_JACK
  tX_jack_client::init();
#endif

#ifdef USE_JACK
  jack_check();
#endif

#ifdef USE_ALSA_MIDI_IN
  if (globals.auto_assign_midi) tX_midiin::auto_assign_midi_mappings(NULL, NULL);
#endif

  printf("Done.\n");
}


void close_terminatorX(void)
{
  printf("Close... ");

  store_globals();

  delete engine;

#ifdef USE_JACK
  if (tX_jack_client::get_instance()) {
    delete tX_jack_client::get_instance();
  }
#endif // USE_JACK

  printf("Done.\n");
}

void use_audiodevice_type (const char * descr, tX_audiodevice_type type)
{
  globals.audiodevice_type = type;

  printf ("Using %s audio device\n", descr);
}

void use_alsa (void)
{
  use_audiodevice_type ("alsa", ALSA);
}

void use_oss (void)
{
  use_audiodevice_type ("oss", OSS);
}

void use_jack (void)
{
  use_audiodevice_type ("jack", JACK);
}



vtt_class * add_vtt(char *filename)
{
  vtt_class *new_tt;

  new_tt = new vtt_class(false);
  if (filename) new_tt->load_file(filename);

  return new_tt;
}





int tty_sequencer_ready=1;
//static bool tty_stop_override=false;

void audio_on(void)
{
  tX_engine_error res;

  tty_sequencer_ready=0;
  //mg_enable_critical_buttons(0);

  res=tX_engine::get_instance()->run();

  if (res!=NO_ERROR) {
    //mg_enable_critical_buttons(1);
    //    tty_stop_override=true;
    //gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w), 0);
    //    tty_stop_override=false;

    switch(res) {
    case ERROR_BUSY:
      tx_note("Error starting engine: engine is already running.", true);
      break;
    case ERROR_AUDIO:
      tx_note("Error starting engine: failed to access audiodevice.\nPlease check the audio device settings in the \"Preferences\" dialog.", true);
      break;
    case ERROR_TAPE:
      tx_note("Error starting engine: failed to open the recording file.", true);
      break;
    default:tx_note("Error starting engine: Unknown error.", true);
    }

    return;
  }

  if (tX_engine::get_instance()->get_recording_request()) {
    printf ("Start recording to %s\n", globals.record_filename);
  }


  tty_sequencer_ready=1;
  //stop_update=0;
  audioon=1;
  //update_delay=globals.update_delay;
  //update_tag=gtk_timeout_add(globals.update_idle, (GtkFunction) pos_update, NULL);
  //gtk_widget_set_sensitive(grab_button, 1);
}




//void tty_seq_stop(void)
//{
  //  if (!sequencer_ready) return;
  //  sequencer.trig_stop();
  //seq_adj_care=1;
  //seq_stop_override=1;
  //gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(seq_play_btn), 0);
  //gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(seq_rec_btn), 0);
  //while (gtk_events_pending()) gtk_main_iteration();
  //seq_stop_override=0;
  //gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(engine_btn), 0);
  //gtk_widget_set_sensitive(seq_slider, 1);
  //gtk_widget_set_sensitive(engine_btn, 1);
  //gtk_widget_set_sensitive(seq_rec_btn, 1);
//}


void audio_off(void)
{
  //if (tty_stop_override) return;
  if (!tty_sequencer_ready) return;
  //gtk_widget_set_sensitive(grab_button, 0);
  tX_engine::get_instance()->stop();
  //stop_update=1;
  audioon=0;
//  if (tX_engine::get_instance()->get_recording_request()) {
//    tX_engine::get_instance()->set_recording_request(false);
//    //rec_dont_care=1;
//    //gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(rec_menu_item), 0);
//    //rec_dont_care=0;
//  }

  //tty_seq_stop();
  //mg_enable_critical_buttons(1);

  if (tX_engine::get_instance()->get_runtime_error()) {
    tx_note("Fatal: The audio device broke down while playing\nback audio. Note that that some audio devices can not\nrecover from such a breakdown.", true);
  }
  if (tX_engine::get_instance()->get_overload_error()) {
    tx_note("Fatal: The audio engine was stopped due to an overload\ncondition. Try reducing the amount of plugins or\nturntables.", true);
  }
}


void rec_on (void)
{
  printf ("Recording to %s\n", globals.record_filename);
  tX_engine::get_instance()->set_recording_request(true);
  //rec_dont_care=0;
}


void rec_off (void)
{
  if (tX_engine::get_instance()->get_recording_request()) {
    printf ("Stop recording to %s\n", globals.record_filename);
    tX_engine::get_instance()->set_recording_request(false);
    //rec_dont_care=1;
  }
}

void rec_to (char *filename)
{
  strncpy (globals.record_filename, filename, PATH_MAX);
  printf ("Set record filename to %s\n", globals.record_filename);
}



void remove_all_vtt (void)
{
  vtt_class::delete_all();

#ifdef USE_ALSA_MIDI_IN
  if (globals.auto_assign_midi) tX_midiin::auto_assign_midi_mappings(NULL, NULL);
#endif
}





void set_volume(vtt_class *vtt, f_prec vol)
{
  vtt->set_volume(vol);
  // Additionally, the sample position is reseted each time volume is at zero.
  if (vol == 0) {
    if (vtt->is_playing) vtt->stop();
  } else {
    if (!vtt->is_playing) vtt->trigger();
  }
}

void set_pan(vtt_class *vtt, f_prec pan)
{
  vtt->set_pan(pan);
}


void set_pitch(vtt_class *vtt, f_prec pitch)
{
  vtt->set_pitch(pitch);
}


// Builtin Low Pass Filter
void set_lp_enable(vtt_class *vtt, f_prec enable)
{
  if (enable >= 1) {
    if (! vtt->lp_enable)
      vtt->lp_set_enable(1);
  } else {
    if (vtt->lp_enable)
      vtt->lp_set_enable(0);
  }
}

void set_lp_gain(vtt_class *vtt, f_prec gain)
{
  vtt->lp_set_gain(gain);
}

void set_lp_reso(vtt_class *vtt, f_prec reso)
{
  vtt->lp_set_reso(reso);
}

void set_lp_freq(vtt_class *vtt, f_prec freq)
{
  vtt->lp_set_freq(freq);
}


// Builtin Echo
void set_ec_enable(vtt_class *vtt, f_prec enable)
{
  if (enable >= 1) {
    if (! vtt->ec_enable)
      vtt->ec_set_enable(1);
  } else {
    if (vtt->ec_enable)
      vtt->ec_set_enable(0);
  }
}

void set_ec_length(vtt_class *vtt, f_prec length)
{
  vtt->ec_set_length(length);
}

void set_ec_feedback(vtt_class *vtt, f_prec feedback)
{
  vtt->ec_set_feedback(feedback);
}

void set_ec_volume(vtt_class *vtt, f_prec volume)
{
  vtt->ec_set_volume(volume);
}

void set_ec_pan(vtt_class *vtt, f_prec pan)
{
  vtt->ec_set_pan(pan);
}


// LADSPA Plugins

void list_plugins (void) {
  LADSPA_Class::dump();
}


static list <tX_seqpar_vttfx *> :: iterator sp;
static vtt_fx_ladspa *new_effect;
static vtt_fx_stereo_ladspa *new_stereo_effect;

static int is_stereo;

int add_vtt_fx(vtt_class *vtt, long int ID)
{
  LADSPA_Plugin *plugin;
  LADSPA_Stereo_Plugin *stereo_plugin;
  int i;

  is_stereo = 0;

  plugin = LADSPA_Plugin :: getPluginByUniqueID (ID);

  if (!plugin) {
    printf ("[%d] is a stereo effect.\n", ID);
    new_effect = NULL;
    stereo_plugin = LADSPA_Stereo_Plugin :: getPluginByUniqueID (ID);
    if (!stereo_plugin)
      {
	printf ("[%d] plugin not found!\n");
	is_stereo = -1;
	return 0;
      }
    is_stereo = 1;
  }

  if (is_stereo != 1)
    {
      new_effect = vtt->add_effect(plugin);
      sp=new_effect->controls.begin();
    }
  else
    {
      new_stereo_effect = vtt->add_stereo_effect(stereo_plugin);
      sp=new_stereo_effect->controls.begin();
    }

  return -1;
}

tX_seqpar_vttfx * get_next_fx_control(void)
{
  switch (is_stereo) {
  case -1 : printf("Not a valid plugin\n"); return NULL;
  case 0  :
    if (sp == new_effect->controls.end())
      {
	printf ("Mono: End of controls\n");
	return NULL;
      }
    break;
  case 1 :
    if (sp == new_stereo_effect->controls.end())
      {
	printf ("Stereo: End of controls\n");
	return NULL;
      }
    break;
  }

  (*sp)->do_exec((*sp)->get_min_value());
  printf("  Adding control: %s\n", (*sp)->get_name());

  sp++;

  return (*sp);
}


const char * get_fx_control_name (tX_seqpar_vttfx * sp)
{
  return sp->get_name();
}

const char * get_fx_control_label_name (tX_seqpar_vttfx * sp)
{
  return sp->get_label_name();
}


float get_fx_control_value (tX_seqpar_vttfx * sp)
{
  return sp->get_value();
}

float get_fx_control_min_value (tX_seqpar_vttfx * sp)
{
  return sp->get_min_value();
}

float get_fx_control_max_value (tX_seqpar_vttfx * sp)
{
  return sp->get_max_value();
}

float get_fx_control_scale_value (tX_seqpar_vttfx * sp)
{
  return sp->get_scale_value();
}

void set_fx_control_value (tX_seqpar_vttfx * sp, float value)
{
  sp->do_exec(value);
}

