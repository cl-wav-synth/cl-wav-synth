#ifndef __TTY_LIB
#define __TTY_LIB

#include "tX_types.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

  int my_cpp_function (int x);

  void init_terminatorX(void);
  void close_terminatorX(void);

  vtt_class * add_vtt(char *filename);
  void remove_all_vtt (void);


  void use_alsa (void);
  void use_oss (void);
  void use_jack (void);


  void audio_on (void);
  void audio_off (void);

  void rec_on (void);
  void rec_off (void);
  void rec_to (char *filename);

  void list_plugins (void);

  void set_volume(vtt_class *vtt, f_prec vol);
  void set_pan(vtt_class *vtt, f_prec pan);
  void set_pitch(vtt_class *vtt, f_prec pitch);

  // Builtin Low Pass filter
  void set_lp_enable(vtt_class *vtt, f_prec pitch);
  void set_lp_gain(vtt_class *vtt, f_prec gain);
  void set_lp_reso(vtt_class *vtt, f_prec reso);
  void set_lp_freq(vtt_class *vtt, f_prec freq);

  // Builtin Echo
  void set_ec_enable(vtt_class *vtt, f_prec enable);
  void set_ec_length(vtt_class *vtt, f_prec length);
  void set_ec_feedback(vtt_class *vtt, f_prec feedback);
  void set_ec_volume(vtt_class *vtt, f_prec volume);
  void set_ec_pan(vtt_class *vtt, f_prec pan);

  // LADSPA Plugins
  int add_vtt_fx(vtt_class *vtt, long int ID);
  tX_seqpar_vttfx * get_next_fx_control(void);

  const char * get_fx_control_name (tX_seqpar_vttfx * sp);
  const char * get_fx_control_label_name (tX_seqpar_vttfx * sp);
  float get_fx_control_value (tX_seqpar_vttfx * sp);
  float get_fx_control_min_value (tX_seqpar_vttfx * sp);
  float get_fx_control_max_value (tX_seqpar_vttfx * sp);
  float get_fx_control_scale_value (tX_seqpar_vttfx * sp);
  void set_fx_control_value (tX_seqpar_vttfx * sp, float value);



#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif
