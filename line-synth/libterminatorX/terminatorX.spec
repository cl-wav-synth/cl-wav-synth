%define name terminatorX
%define version 3.82
%define release 1

Summary: Realtime Audio Synthesizer
Name: %{name}
Version: %{version}
Release: %{release}
Group: Applications/Multimedia
Copyright: GPL2
URL: http://www.terminatorX.cx
Source: %{name}-%{version}.tar.gz
Buildroot: %{_tmppath}/%{name}-%{version}-root
Requires: gtk2, libvorbis, audiofile, libxml2, zlib, scrollkeeper, yelp, sox, vorbis-tools, libcap
BuildPrereq: gtk2-devel, libvorbis-devel, audiofile-devel, libxml2-devel, scrollkeeper, libcap-devel

%description
terminatorX is a realtime audio synthesizer that allows you to "scratch" on 
digitally sampled audio data (*.wav, *.au, *.ogg, *.mp3, etc.) the way 
hiphop-DJs scratch on vinyl records. It features multiple turntables, realtime
effects (buit-in as well as LADSPA plugin effects), a sequencer and MIDI
interface - all accessible through an easy-to-use gtk+ GUI.

%changelog
* Mon Aug 18 2003 Alexander K�nig <alex@lisas.de>
 - added libcap dependency
* Fri Apr 11 2003 Alexander K�nig <alex@lisas.de>
 - disabled the suid root stuff
* Fri Apr 11 2003 Alexander K�nig <alex@lisas.de>
 - scrollkeeper changes and updates
* Sat Sep 14 2002 Alexander K�nig <alex@lisas.de>
 - Switch from xpm to pngs for GNOME icons
* Fri May 31 2002 Alexander K�nig <alex@lisas.de>
 - Added Adrian's man page
* Tue Mar 20 2001 Adrian Reber <adrian@lisas.de>
 - Updated to 3.71
* Sat Dec 09 2000 Adrian Reber <adrian@lisas.de>
 - Updated to 3.70
* Wed Apr 12 2000 Adrian Reber <adrian@lisas.de>
 - Updated to 3.60
* Wed Feb 23 2000 Adrian Reber <adrian@42.fht-esslingen.de>
 - Mandrake adaptations.
* Thu Feb 14 2000 Adrian Reber <adrian@42.fht-esslingen.de>
 - Updated to 3.55
* Thu Dec 17 1999 Adrian Reber <adrian@42.fht-esslingen.de>
 - Updated to 3.5
* Thu Jul 29 1999 Adrian Reber <adrian@rhlx01.fht-esslingen.de>
 - Updated to 3.2
* Fri May 07 1999 Adrian Reber <adrian@rhlx01.fht-esslingen.de>
 - Initial release

%prep

%setup -q

%build
%configure --with-docdir=/usr/share

uname -a|grep SMP && make -j 2 || make

%install

%makeinstall

MIMEDIR=$RPM_BUILD_ROOT%{_datadir}/mime-info
ICONDIR=$RPM_BUILD_ROOT%{_datadir}/pixmaps
ICONS="gnome-support/terminatorX-app.png gnome-support/terminatorX-mime.png"
MIMEFILES="gnome-support/terminatorX.keys gnome-support/terminatorX.mime"

mkdir -p $MIMEDIR
mkdir -p $ICONDIR

cp $ICONS $ICONDIR
cp $MIMEFILES $MIMEDIR

( cd $RPM_BUILD_ROOT%{_datadir}
  mkdir -p ./gnome/apps/Multimedia
  cat > ./gnome/apps/Multimedia/terminatorX.desktop <<EOF
[Desktop Entry]
Name=terminatorX
Type=Application
Comment=sophisticated scratching and audio tool
Exec=terminatorX
Icon=terminatorX-app.png
EOF
)

# cleanup that scrollkeeper stuff
rm -rf $RPM_BUILD_ROOT/var

#do some cleanup for the documentation
#find . -name "Makefile*" -print -exec rm -rf {} \;

%files
%defattr(0644,root,root,0755)
%doc COPYING COPYING-DOCS AUTHORS ChangeLog INSTALL NEWS README THANKS TODO README.GNOME README.PERFORMANCE
%attr(0755,root,root) %{_bindir}/terminatorX
%{_mandir}/man1/terminatorX.1*
%{_datadir}/mime-info/terminatorX.keys
%{_datadir}/mime-info/terminatorX.mime
%{_datadir}/pixmaps/terminatorX-app.png
%{_datadir}/pixmaps/terminatorX-mime.png
%{_datadir}/gnome/apps/Multimedia/terminatorX.desktop
%{_datadir}/omf/terminatorX
%{_datadir}/terminatorX

%post
if which scrollkeeper-update>/dev/null 2>&1; then scrollkeeper-update -q -o %{_datadir}/omf/terminatorX; fi

echo Note: Installing terminatorX \"suid root\" improves terminatorX\' performance 
echo for non-root users, but poses a potential security threat. See \'INSTALL\' for
echo and \'README.PERFORMANCE\' details. If you want to take the risk (after you 
echo have read the docs) you can execute the following command:
echo
echo                        chmod u+s /usr/bin/terminatorX
echo
echo to install terminatorX "suid root". 

%postun
if which scrollkeeper-update>/dev/null 2>&1; then scrollkeeper-update -q; fi
