There are several things you can do to improve terminatorX performance:

1. Don't use fancy gtk+ themes
2. Install suid root
3. Compile an optimized binary
4. Optimize your terminatorX setup
5. Optimize your kernel

In Detail:

1. Don't use pixmapped nor other "fat" gtk+-themes.
---------------------------------------------------

terminatorX involves quite some GUI activity, so it's desirable that drawing
happens fast. Some gtk+ engines can slow down drawing signifcantly

2. Install suid root
--------------------

Note: Installing a program suid-root is always potentially dangerous. However,
a program needs special privileges to acquire realtime scheduling (which 
improves playback quality signifcantly). Since release 3.82 terminatorX provides
only one method of running suid root:

- Linux' POSIX capabilities: when capabilities support is available at
  compile time (requires libcap) terminatorX can make use of Linux' POSIX
  capabilities. The binary has to be installed suid root but the program
  will drop root privileges right on startup - after aquiring the CAP_SYS_NICE
  capability.

While the capabilities based approach seems much more secure than the approach
that was previously implemented, it might still be exploitable. So, for 100%
security you have to do without realtime scheduling and not install terminatorX
suid root.

3. Compile an optimized binary
------------------------------

This issue is covered in the INSTALL file mostly. Setting good optimization
flags is a good idea although this step will proabably have the least effect.

4. Optimize your terminatorX setup
----------------------------------

The default settings for the GUI updates provide good realtime feedback. This
may cause problems on slower machines or evil gtk+-themes. If the GUI-thread
causes dropouts in the audio-engine you should increase the Update-Delay value 
in the options dialog.

5. Optimize your kernel
-----------------------

2.6 Kernels have much lower latency than previous versions - if you prefer 2.4
Kernels however, I recommend patching the 2.4 Series with Con Kolivas' "ck" 
patches. They improve terminatorX performace signifcantly - if you dont forget 
to renice your X to '0' (see Con Kolivas' FAQ at 
http://members.optusnet.com.au/ckolivas/kernel/).
