(defparameter *base-dir* (directory-namestring *load-truename*))

(push *base-dir* asdf:*central-registry*)

(asdf:oos 'asdf:load-op :line-synth)

(setf *print-circle* t)

(in-package :line-synth)

(run)

(in-package :common-lisp-user)

(quit)
