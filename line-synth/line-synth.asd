;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-

(defsystem line-synth
    :description "A line synth"
    :author "Philippe Brochard <pbrochard@common-lisp.net>"
    :version "0.0"
    :licence "GNU Public License (GPL)"
    :components
    ((:module "src"
              :components
              ((:file "package")
               (:file "tools"
                      :depends-on ("package"))
               (:file "libterminatorX"
                      :depends-on ("package" "tools"))
               (:file "line"
                      :depends-on ("package" "tools" "libterminatorX"))
               (:file "timer"
                      :depends-on ("package"))
               (:file "window"
                      :depends-on ("package" "tools" "line" "timer"))
               (:file "vtt"
                      :depends-on ("package" "window" "line" "timer"))
               (:file "keysyms"
                      :depends-on ("package"))
               (:file "keys"
                      :depends-on ("package" "tools" "keysyms"))
               (:file "mouse-actions"
                      :depends-on ("package" "tools" "line" "window"))
               (:file "actions"
                      :depends-on ("package" "tools" "vtt" "line" "window"))
               (:file "repl"
                      :depends-on ("package" "vtt" "libterminatorX" "actions"))
               (:file "bindings"
                      :depends-on ("package" "keys"))
               (:file "line-synth"
                      :depends-on ("libterminatorX" "package" "line" "timer" "window" "vtt" "repl" "keys"))))))


